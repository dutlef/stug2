import socket
import bpy

# UDP_IP = "127.0.0.1"
UDP_PORT = 34254
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def send_cmd(cmd_name, *args):
    data_string = cmd_name
    UDP_IP = bpy.data.scenes[0].sync_properties.ip_address
    for arg in args:
        data_string += "#" + arg
    # print("sending: " + data_string)
    sock.sendto(bytes(data_string, "utf-8"), (UDP_IP, UDP_PORT))