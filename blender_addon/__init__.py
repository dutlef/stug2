# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy

bl_info = {
    "name" : "Blender2VR",
    "author" : "Dorian Zgraggen",
    "description" : "",
    "blender" : (2, 80, 0),
    "version" : (0, 0, 1),
    "location" : "",
    "warning" : "",
    "category" : "Generic"
}

from . import auto_load

auto_load.init()

def register():
    auto_load.register()
    # for panel in get_panels():
    #     panel.COMPAT_ENGINES.add('CUSTOM')

def unregister():
    auto_load.unregister()
    # for panel in get_panels():
    #     if 'CUSTOM' in panel.COMPAT_ENGINES:
    #         panel.COMPAT_ENGINES.remove('CUSTOM')



# def get_panels():
#     exclude_panels = {
#         'VIEWLAYER_PT_filter',
#         'VIEWLAYER_PT_layer_passes',
#     }

#     panels = []
#     for panel in bpy.types.Panel.__subclasses__():
#         if hasattr(panel, 'COMPAT_ENGINES') and 'BLENDER_RENDER' in panel.COMPAT_ENGINES:
#             if panel.__name__ not in exclude_panels:
#                 panels.append(panel)

#     return panels