import bpy
import numpy
import socketserver
from threading import Thread
import mathutils
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import os
import socket

from pathlib import Path

from .udp_bridge import send_cmd
from .object import defaults

# TODO: don't allow start when already running
# TODO: don't allow stop when nothing is running

timer = None
thread = None
httpd = None
running = False
wm = None

PORT = 8517
DIRECTORY = ""

device_status = "unknown"


class B2VR_Start(bpy.types.Operator):
    """Start VR and synchronize stuff"""
    bl_idname = "b2vr.start"
    bl_label = "Start VR"

    @classmethod
    def poll(cls, context):
        return True


    def modal(self, context, event):
        # if event.type in {'RIGHTMOUSE', 'ESC'}:
        #     self.cancel(context)
        #     return {'CANCELLED'}

        if event.type == 'TIMER':
            for area in bpy.context.screen.areas:
                if area.type == 'VIEW_3D':
                    r3d = area.spaces.active.region_3d
                    vm = r3d.view_matrix
                    matrix = numpy.array(vm).tolist()
                    send_cmd("view_matrix", str(matrix))

                    q = r3d.view_rotation
                    euler = q.to_euler()
                    euler_godot = [euler.x, euler.z, -euler.y]
                    # print(euler)
                    location = r3d.view_matrix.inverted().translation
                    # https://www.gamedev.net/forums/topic/56471-extracting-direction-vectors-from-quaternion/

                    # view direction
                    x = -2 * (q.x*q.z + q.w*q.y)
                    y = -2 * (q.y*q.z - q.w*q.x)
                    z = -(1 - 2 * (q.x*q.x + q.y*q.y))

                    godot_dirs = [x, z, -y]

                    # up direction
                    x = 2 * (q.x * y - q.w * q.z)
                    y = 1 - 2 * (q.x * q.x + q.z * q.z)
                    z = 2 * (q.y * q.z + q.w * q.x)

                    # print("up" + str([x,y,z]))

                    godot_dirs.extend([x, z, -y])

                    # print("dirs: " + str(godot_dirs))

                    # view_dir = r3d.view_rotation * mathutils.Vector((0.0, 0.0, -1.0))
                    # up_dir = r3d.view_rotation * mathutils.Vector((0.0, 1.0, -1.0))


                    # blender => godot
                    #    x    =>   x
                    #    y    =>  -z
                    #    z    =>   y
                    location_godot = [location[0], location[2], -location[1]]
                    # quat_godot = [rotation.x, rotation.y, rotation.z, rotation.w]
                    # godot_dir = [
                    #     view_dir[0], view_dir[2], -view_dir[1],
                    #     up_dir[0], up_dir[2], -up_dir[1]
                    # ]

                    # print("location" + str(location))
                    # print("rotation" + str(rotation))

                    send_cmd("camera_location", str(location_godot))
                    send_cmd("camera_directions", str(godot_dirs))
                    # send_cmd("camera_euler", str(euler_godot))
                    # send_cmd("camera_quaternion", str(quat_godot))
                    # send_cmd("camera_direction", str(godot_dir))
                    break

        return {'PASS_THROUGH'}

    def execute(self, context):
        global timer
        global thread
        global running

        if running:
            return {'CANCELLED'}
        
        running = True

        # ping_device(self, context)

        wm = context.window_manager
        # timer = wm.event_timer_add(0.03, window=context.window)
        # wm.modal_handler_add(self)

        thread = Thread(
            target=start_server,
            args=[bpy.path.abspath("//b2vr_data")]
        )
        thread.start()
            
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        global wm
        wm = context.window_manager
        wm.event_timer_remove(timer)


class B2VR_Stop(bpy.types.Operator):
    """Stop VR """
    bl_idname = "b2vr.stop"
    bl_label = "Stop VR"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        global wm
        wm = context.window_manager
        stop()
        return {'FINISHED'}

def stop():
    global timer
    global running
    global wm

    if not running:
        return {'CANCELLED'}
    
    running = False

    # if wm is not None:
    #     wm.event_timer_remove(timer)

    if httpd is not None:
        httpd.shutdown()
        httpd.server_close()

def menu_func(self, context):
    self.layout.operator(B2VR_Start.bl_idname, text=B2VR_Start.bl_label)
    self.layout.operator(B2VR_Stop.bl_idname, text=B2VR_Stop.bl_label)


def register():
    bpy.types.VIEW3D_MT_object.append(menu_func)
    bpy.types.VIEW3D_MT_object.append(menu_func)
    bpy.types.Scene.sync_properties = bpy.props.PointerProperty(type=SyncProperties)


def unregister():
    stop()
    bpy.types.VIEW3D_MT_object.remove(menu_func)
    # bpy.utils.unregister_class(SimpleOperator)
    bpy.types.VIEW3D_MT_object.remove(menu_func)
    del bpy.types.Scene.sync_properties



# SERVER ==============

def start_server(directory):
    global DIRECTORY
    global httpd
    DIRECTORY = directory
    httpd = HTTPServer(('0.0.0.0', PORT), Handler)
    print("serving at port", PORT)
    httpd.serve_forever()

class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path.startswith('/static/'):
            self.serve_static()
        else:
            self.serve_dynamic()

    def serve_static(self):
        global DIRECTORY

        try:
            path = self.path[8:]  # remove '/static/' from the path
            with open(DIRECTORY + '/' + path, 'rb') as f:
                self.send_response(200)
                
                # not exactly sure which content-type to put here
                self.send_header('Content-type', 'application/octet-stream')

                self.send_header('B2VR-File-Name', path)
                
                # TODO: research: godot needs this header, but maybe it needs it only after
                # a certain file size, because it worked without it in the beginning)
                fs = os.fstat(f.fileno())
                self.send_header("Content-Length", str(fs[6]))

                self.end_headers()
                self.wfile.write(f.read())
        except FileNotFoundError:
            self.send_response(404)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(b'404 file does not exist')

    def serve_dynamic(self):
        match self.path:
            case "/connect":
                bg = bpy.data.worlds[0].b2vr_custom_world_properties.background_color

                data = {
                    'background_color': {
                        'r': bg[0],
                        'g': bg[1],
                        'b': bg[2],
                    }
                }
                encoded_data = json.dumps(data).encode()
                content_length = len(encoded_data)

                self.send_response(200)
                self.send_header('Content-type', 'application/json')
                self.send_header('Content-Length', str(content_length))
                self.end_headers()
                
                self.wfile.write(encoded_data)
                return
            
            case "/defaults":
                encoded_data = json.dumps(defaults).encode()
                content_length = len(encoded_data)

                self.send_response(200)
                self.send_header('Content-type', 'application/json')
                self.send_header('Content-Length', str(content_length))
                self.end_headers()
                
                self.wfile.write(encoded_data)
                return

        self.send_response(404)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(b'404 invalid request')


def main(context):
    # for ob in context.scene.objects:
    #     print(ob)

    folder = bpy.path.abspath("//b2vr_data")
    filepath = bpy.path.abspath("//b2vr_data/scene.gltf")

    Path(folder).mkdir(parents=True, exist_ok=True)

    bpy.ops.export_scene.gltf(
        filepath=filepath,
        # export_format='GLB',
        export_format='GLTF_SEPARATE',
        use_visible=True,
        export_apply=True,
        export_materials="NONE",
        export_attributes=True,
        use_active_scene=True,
        export_extras=True,
    )

    send_cmd("sync", get_ip())

# https://stackoverflow.com/a/28950776
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.settimeout(0)
    try:
        # doesn't even have to be reachable
        s.connect(('10.254.254.254', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP


# TODO: expect custom response instead of pinging
def ping_device(self, context):
    global device_status
    hostname = context.scene.sync_properties.ip_address
    try:
        response = os.system(f"ping -c 1 {hostname}")
        print(response)
        if response == 0:
            device_status = "valid"
        else:
            device_status = "invalid"
    except:
        device_status = "invalid_address"

class SimpleOperator(bpy.types.Operator):
    """Synchronizes the Blender scene with the scene on your VR headset"""
    bl_idname = "b2vr.sync"
    bl_label = "Sync to VR"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        main(context)
        return {'FINISHED'}

def menu_func(self, context):
    self.layout.operator(SimpleOperator.bl_idname, text=SimpleOperator.bl_label)


class SyncProperties(bpy.types.PropertyGroup):
    ip_address: bpy.props.StringProperty(
        name="Headset IP address",
        description="Enter the address displayed on your headset",
        default="",
        # update=ping_device
    )


class SyncPanel(bpy.types.Panel):
    bl_label = "Connection"
    bl_category = "Blender2VR"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw(self, context):
        layout = self.layout

        if running:
            row = layout.row()
            row.label(text="Headset IP Address")

            row = layout.row()
            row.prop(context.scene.sync_properties, "ip_address", text="")

            if device_status == "unknown":
                layout.row().label(text="Headset status unknown.", icon="ERROR")
            elif device_status == "invalid":
                layout.row().label(text="Could not ping headset.", icon="ERROR")
            elif device_status == "invalid_address":
                layout.row().label(text="Invalid address.", icon="ERROR")
            elif device_status == "valid":
                layout.row().label(text="Connected to headset.", icon="CHECKMARK")

            row = layout.row()
            row.operator("b2vr.sync", text="Sync Scene to VR", icon="META_CUBE")

            row = layout.row()
            row.operator("b2vr.stop", text="STOP Server", icon="SNAP_FACE")
        else:
            row = layout.row()
            row.operator("b2vr.start", text="Start Server", icon="PLAY")


        # DISCLOSURE_TRI_RIGHT
        # META_CUBE
        # ARROW_LEFTRIGHT

