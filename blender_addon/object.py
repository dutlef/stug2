# bpy.data.scenes[0].sync_properties.ip_address
import bpy
import mathutils
from .udp_bridge import send_cmd


class ObjectMaterialPanel(bpy.types.Panel):
    bl_label = "Material"
    bl_idname = "B2VR_Material_Panel"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "material"

    def draw(self, context):
        layout = self.layout
        layout.label(text="bonjour")
        layout.prop(context.object.b2vr_custom_object_properties, "object_color", text="Color")

def handle_update(self, context):
    context.object.color[0] = self.object_color.r
    context.object.color[1] = self.object_color.g
    context.object.color[2] = self.object_color.b


def on_is_nav_mesh_update(self, context):
    if bpy.context.object.b2vr_custom_object_properties.is_nav_mesh:
        bpy.context.object.b2vr_custom_object_properties.initial_display_type = bpy.context.object.display_type
        bpy.context.object.display_type = 'WIRE'
    else:
        bpy.context.object.display_type = bpy.context.object.b2vr_custom_object_properties.initial_display_type

class CustomObjectProperties(bpy.types.PropertyGroup):
    object_color: bpy.props.FloatVectorProperty(
        name="Custom Color",
        subtype='COLOR',
        default=(1.0, 1.0, 1.0),
        min=0.0,
        max=1.0,
        update=handle_update
    )


    texture_path: bpy.props.StringProperty(
        name = "Texture",
        subtype = 'FILE_PATH'
    )

    use_gravity: bpy.props.BoolProperty(
        name="Enable Gravity",
    )

    is_collider: bpy.props.BoolProperty(
        name="Enable Collisions",
    )
    grabbable: bpy.props.BoolProperty(
        name="Grabbable",
    )

    remote_grab: bpy.props.BoolProperty(
        name="Remote Grab",
    )

    is_nav_mesh: bpy.props.BoolProperty(
        name="Enable Navigation Mesh",
        update=on_is_nav_mesh_update
    )

    initial_display_type: bpy.props.StringProperty(
        name="Animation Name",
    )


    # these fake_ properties are used for when i want to set properties to a specific value
    # depending on anither property, but i still want to keep the original data intact
    fake_use_gravity: bpy.props.BoolProperty(
        name="Gravity",
        default=False
    )
    fake_is_collider: bpy.props.BoolProperty(
        name="Collisions",
        default=True
    )




# TODO: use global json file or similar to configure settings, because
# default values in custom properties are not exported
    
# (identifier, name, description, icon, number)
# https://docs.blender.org/api/current/bpy.props.html#bpy.props.EnumProperty
data_for_enum = [
    ("A", "None", "lalla"),
    ("B", "Change Color", "change color haha"),
    ("C", "Float", "makes the thingy float"),
    ("D", "Play animation", "play an animation"),
    ("E", "Play Sound", "play a sound"),
]



triggers = [
    "On Begin",
    "On Proximity Enter",
    "On Proximity Leave",
    "On Touch",
    "On Pointing At",
    "On Looking At",
]

defaults = {
    "color": (1.0, 1.0, 1.0),
    "transition_duration": 0.5,
    "extent": 0.5,
    "speed": 1,
    "looking_at_distance": 3
}

def trigger_name_to_id(trigger_name):
    return "b2vr_" + trigger_name.lower().replace(" ", "_")

def triggers_snake_case():
    trigger_lower = []
    for trigger in triggers:
        id = trigger_name_to_id(trigger)
        trigger_lower.append(id)
    return trigger_lower

def draw_trigger_settings(layout, self, context, property, i):
    row = layout.row()
    row.label(text="trigger_settings")

def draw_interaction_config_box(layout, context, trigger_name):
    row = layout.row()
    p = getattr(context.object, trigger_name_to_id(trigger_name))
    interaction = getattr(p, "presets")

    row.prop(p, 'presets', text= 'Reaction')

    if interaction == "A":
        return

    box = layout.box()

    if interaction == "B":
        row = box.row()
        row.prop(p, "color")
        row = box.row()
        row.prop(p, "transition_duration", text= "Transition Duration")


    if interaction == "C":
        row = box.row()
        col = row.column(align=True)
        col.prop(p, "extent")
        col.prop(p, "speed")

    if interaction == "D":
        row = box.row()
        col = row.column(align=True)
        col.prop(p, "animation_id")
        col.prop(p, "animation_name")

    if interaction == "E":
        row = box.row()
        row.label(text="Todo: sound")


class B2VR_PT_ObjectSettings(bpy.types.Panel):
    bl_idname = 'B2VR_PT_ObjectSettings'
    bl_label = "Object Settings"
    bl_category = "Blender2VR"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw(self, context):
        layout = self.layout

        layout.use_property_split = True
        layout.use_property_decorate = False # not keyframable

        layout.prop(context.object.b2vr_custom_object_properties, "object_color", text="Color")
        layout.prop(context.object.b2vr_custom_object_properties, "texture_path")

        # layout.use_property_split = False


        if context.object.type == "MESH":

            col = layout.column(heading="Enable")
            col.prop(context.object.b2vr_custom_object_properties, "is_nav_mesh", text="Navigation Mesh")


            if context.object.b2vr_custom_object_properties.is_nav_mesh:
                row = col.row()
                row.enabled = False
                row.prop(context.object.b2vr_custom_object_properties, "fake_is_collider")
                row = col.row()
                row.enabled = False
                row.prop(context.object.b2vr_custom_object_properties, "fake_use_gravity")

            else:
                col.prop(context.object.b2vr_custom_object_properties, "is_collider", text="Collisions")
                col.prop(context.object.b2vr_custom_object_properties, "use_gravity", text="Gravity")


            col.prop(context.object.b2vr_custom_object_properties, "grabbable", text="Grabbing")
            if context.object.b2vr_custom_object_properties.grabbable:
                col.prop(context.object.b2vr_custom_object_properties, "remote_grab", text="Remote Grabbing")

        else:
            layout.label(text="Select a mesh to configure more settings.")

class B2VR_PT_Interactions(bpy.types.Panel):
    bl_idname = 'B2VR_PT_Interactions'
    bl_label = "Triggers & Reactions"
    bl_category = "Blender2VR"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw(self, context):
        pass



def configure_layout(self):
    layout = self.layout
    layout.use_property_split = True
    layout.use_property_decorate = False # not keyframable
    return layout

def draw_object_selector(layout, context, trigger):
    p = getattr(context.object, trigger_name_to_id(trigger))

    layout.prop(p, "object_type")
    if p.object_type == "custom":
        layout.prop(p, "object_pointer", text="Custom Object")


class B2VR_PT_Interactions_OnBegin(bpy.types.Panel):
    bl_idname = 'B2VR_PT_Interactions_OnBegin'
    bl_label = "On Begin"
    bl_space_type = "VIEW_3D"
    bl_parent_id = "B2VR_PT_Interactions"
    bl_region_type = "UI"

    def draw(self, context):
        layout = configure_layout(self)
        draw_interaction_config_box(layout, context, self.bl_label)


class B2VR_PT_Interactions_OnProximityEnter(bpy.types.Panel):
    bl_idname = 'B2VR_PT_Interactions_ProximityEnter'
    bl_label = "On Proximity Enter"
    bl_space_type = "VIEW_3D"
    bl_parent_id = "B2VR_PT_Interactions"
    bl_region_type = "UI"

    def draw(self, context):
        layout = configure_layout(self)
        draw_object_selector(layout, context, self.bl_label)
        draw_interaction_config_box(layout, context, self.bl_label)


class B2VR_PT_Interactions_OnProximityLeave(bpy.types.Panel):
    bl_idname = 'B2VR_PT_Interactions_OnProximityLeave'
    bl_label = "On Proximity Leave"
    bl_space_type = "VIEW_3D"
    bl_parent_id = "B2VR_PT_Interactions"
    bl_region_type = "UI"

    def draw(self, context):
        layout = configure_layout(self)
        draw_object_selector(layout, context, self.bl_label)
        draw_interaction_config_box(layout, context, self.bl_label)


class B2VR_PT_Interactions_OnLookingAt(bpy.types.Panel):
    bl_idname = 'B2VR_PT_Interactions_OnLookingAt'
    bl_label = "On Looking At"
    bl_space_type = "VIEW_3D"
    bl_parent_id = "B2VR_PT_Interactions"
    bl_region_type = "UI"

    def draw(self, context):
        layout = configure_layout(self)
        layout.prop(context.object.b2vr_on_looking_at, "looking_at_distance")
        draw_interaction_config_box(layout, context, self.bl_label)



data_for_enum_animation = [('0', 'Action', 'lol ey'), ('1', 'dude.wavywavy', 'lol ey'), ('2', 'weird.jump', 'lol ey')]

animation_enum_data = []

def calculate_animation_names_for_enum():
    global animation_enum_data
    animation_enum_data = []
    for (i, action) in enumerate(bpy.data.actions):
        # print(i)
        # print(action.name)
        animation_enum_data.append((str(i), action.name, "lol ey"))



def lol():
    print("sw hdwh duiw ehdeuwi ================0")
    print(hasattr(bpy.data, "actions"))
    if True:
        return [('0', 'Action2', 'lol ey'), ('1', 'dude.wavywavy2', 'lol ey'), ('2', 'weird.jump2', 'lol ey')]


    # print(bpy.data.actions)
    return [("aa", "no choice", "hahaha")]

def get_animation_names_for_enum(self, context):
    items = []
    for action in bpy.data.actions:
        items.append((action.name, action.name, ""))
    return items

def set_animation_string(self, context):
    for trigger in triggers_snake_case():
        try:
            id = context.active_object[trigger]['animation_id']
            animation_name = bpy.data.actions[id].name
            print("changed to name: " + animation_name)
            bpy.context.active_object[trigger]['animation_name'] = animation_name
        except:
            continue

def update_selected_object(self, context):
    print("====== pointer")
    # print(context.object.object_pointer)
    for trigger in triggers_snake_case():
        try:
            object_pointer = context.active_object[trigger]['object_pointer']
            object_pointer = context.active_object[trigger]['object_name'] = object_pointer.name
        except:
            continue
        


class B2VR_PR_InteractionProperty(bpy.types.PropertyGroup):
        presets: bpy.props.EnumProperty(items = data_for_enum, name = 'Position Preset')

        color: bpy.props.FloatVectorProperty(
            name="Color",
            subtype='COLOR',
            default=defaults["color"],
            min=0.0,
            max=1.0,
        )

        transition_duration: bpy.props.FloatProperty(
            name="Transition Duration",
            default=defaults["transition_duration"],
            min=0.0,
        )

        extent: bpy.props.FloatProperty(
            name="Extent",
            default=defaults["extent"],
            min=0.0,
        )

        speed: bpy.props.FloatProperty(
            name="Speed",
            default=defaults["speed"],
            min=0.0,
        )

        animation_name: bpy.props.StringProperty(
            name="Animation Name",
        )

        animation_id: bpy.props.EnumProperty(
            name="Animation",
            items=get_animation_names_for_enum,
            update=set_animation_string,
        )

        object_type: bpy.props.EnumProperty(
            name="Trigger Object",
            items=[
                ("player", "Player", ""),
                ("custom", "Custom", ""),
            ],
        )

        object_pointer: bpy.props.PointerProperty(
            name="Object",
            type=bpy.types.Object,
            update=update_selected_object
        )

        object_name: bpy.props.StringProperty(
            name="Object Name"
        )

        looking_at_distance: bpy.props.FloatProperty(
            name="Max Distance",
            default=defaults["looking_at_distance"],
            min=0.0,
        )





def register():
    bpy.types.Object.b2vr_custom_object_properties = bpy.props.PointerProperty(type=CustomObjectProperties)
    for trigger in triggers_snake_case():
        # doing this
        setattr(bpy.types.Object, trigger, bpy.props.PointerProperty(type=B2VR_PR_InteractionProperty))
        # because the following doesn't not work:
        # bpy.types.Object[trigger] = bpy.props.PointerProperty(type=B2VR_PR_InteractionProperty)
    

def unregister():
    del bpy.types.Object.b2vr_custom_object_properties
    for trigger in triggers_snake_case():
        delattr(bpy.types.Object, trigger)
        # del bpy.types.Object[trigger]
