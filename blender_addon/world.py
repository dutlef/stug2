import bpy
from .udp_bridge import send_cmd

class WorldBackgroundPanel(bpy.types.Panel):
    bl_label = "Background Settings"
    bl_idname = "WORLD_PT_custom_panel"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "world"

    def draw(self, context):
        layout = self.layout
        world = context.world
        layout.label(text="bonjour")
        layout.prop(world.b2vr_custom_world_properties, "background_color", text="Background Color")

def handle_update(self, context):
    print("update bg", self)
    send_cmd(
        "update_bg",
        str(self.background_color.r), ## TODO: better data structure
        str(self.background_color.g),
        str(self.background_color.b)
    )

class CustomWorldProperties(bpy.types.PropertyGroup):
    background_color: bpy.props.FloatVectorProperty(
        name="Custom Color",
        subtype='COLOR',
        default=(1.0, 1.0, 1.0),
        min=0.0,
        max=1.0,
        update=handle_update
    )



def register():
    print("register_world")
    bpy.types.World.b2vr_custom_world_properties = bpy.props.PointerProperty(type=CustomWorldProperties)


def unregister():
    del bpy.types.World.b2vr_custom_world_properties