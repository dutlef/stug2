#ifndef SceneRoot_H
#define SceneRoot_H

#include "command_processor.h"
#include <godot_cpp/classes/node3d.hpp>
#include <godot_cpp/classes/standard_material3d.hpp>

namespace godot {

class SceneRoot : public Node3D {
	GDCLASS(SceneRoot, Node3D)

protected:
	static void _bind_methods();

public:
	SceneRoot();
	~SceneRoot();

	void _process(double delta) override;
	void _ready() override;

	StandardMaterial3D* grabbable_overlay;

private:
	CmdProcessor* command_processor = NULL;

};
}

#endif
