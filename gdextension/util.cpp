#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <regex>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/variant/string.hpp>
#include "tinyformat/tinyformat.h"

#include <stdlib.h>
#include <sys/types.h>
// #include <ifaddrs.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#include "android-ifaddrs/ifaddrs.h"

#include "util.h"

// https://stackoverflow.com/a/5888676
size_t split_string2(const std::string &txt, std::vector<std::string> &strs, char ch)
{
    size_t pos = txt.find( ch );
    size_t initialPos = 0;
    strs.clear();

    // Decompose statement
    while( pos != std::string::npos ) {
        strs.push_back( txt.substr( initialPos, pos - initialPos ) );
        initialPos = pos + 1;

        pos = txt.find( ch, initialPos );
    }

    // Add the last one
    strs.push_back( txt.substr( initialPos, std::min( pos, txt.size() ) - initialPos + 1 ) );

    return strs.size();
}


int get_local_address() {
    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in *sa;
    char *addr;

    if (getifaddrs(&ifap) == -1) {
        perror("getifaddrs");
        LOG("error");
        return -1;
        // exit(EXIT_FAILURE);
    }

    for (ifa = ifap; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr == NULL || ifa->ifa_addr->sa_family != AF_INET)
            continue;

        sa = (struct sockaddr_in *) ifa->ifa_addr;
        addr = inet_ntoa(sa->sin_addr);
        LOG(tfm::format("%s: %s", ifa->ifa_name, addr));
    }

    freeifaddrs(ifap);
    return 0;
}

int get_broadcast_addr() {
    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in *sa;
    // char *broadcast_ip;
    char broadcast_ip[INET_ADDRSTRLEN];

    if (getifaddrs(&ifap) == -1) {
        std::cerr << "Error getting interface addresses." << std::endl;
        return 1;
    }

    for (ifa = ifap; ifa != nullptr; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr == nullptr || ifa->ifa_addr->sa_family != AF_INET) {
            continue;
        }

        sa = reinterpret_cast<struct sockaddr_in *>(ifa->ifa_addr);

        // Check if interface has netmask
        if (ifa->ifa_netmask == nullptr) {
            std::cerr << "Interface " << ifa->ifa_name << " has no netmask." << std::endl;
            continue;
        }

        struct sockaddr_in *netmask_sa = reinterpret_cast<struct sockaddr_in *>(ifa->ifa_netmask);

        // Calculate the broadcast address
        struct in_addr broadcast_addr;
        broadcast_addr.s_addr = sa->sin_addr.s_addr | (~netmask_sa->sin_addr.s_addr);

        // Convert the broadcast address to string
        inet_ntop(AF_INET, &broadcast_addr, broadcast_ip, INET_ADDRSTRLEN);
        LOG(tfm::format("interface %s: %s", ifa->ifa_name, broadcast_ip));
    }

    freeifaddrs(ifap);
    return 0;
}


std::vector<NetworkInterface> get_data_from_ifconfig_data(const std::string& ifconfig_text) {
    std::vector<NetworkInterface> interfaces;

    std::string result;

    std::istringstream iss(ifconfig_text);

    NetworkInterface current_interface = NetworkInterface();

    for (std::string line; std::getline(iss, line); )
    {
        bool last_iteration = false;
        // std::istringstream line_stream(line);
        // std::string token;
        // line_stream >> token;

        // printf("l: %s \n", line.c_str());
        // printf("empty: %i \n", line.empty());

        if (line.empty())
        {
            current_interface = NetworkInterface();
            continue;
        }

        if (line.at(0) != ' ') // get interface name
        {
            std::string first_word = line.substr(0, line.find(" "));
            current_interface.name = first_word;
        }
        else // find ipv4 address
        { 
            // strip leading whitespace (https://stackoverflow.com/a/21815483)
            std::string stripped = std::regex_replace(line, std::regex("^ +"), "");
            std::string first_word = stripped.substr(0, stripped.find(" "));

            if (first_word == "inet") {
                std::vector<std::string> split;
                split_string2(stripped, split, ' ');
                
                for(auto &str: split) {
                    auto pos = str.find("addr");
                    if (pos == std::string::npos) {
                        continue;
                    }

                    int addr_offset = 5; //offset of 5 removes "addr:" prefix
                    std::string addr = str.substr(pos + addr_offset, str.length());
                    current_interface.inet = addr;
                }
            }

            bool ready_for_push = !current_interface.name.empty() && !current_interface.inet.empty();
            if (ready_for_push) {
                interfaces.push_back(current_interface);
                current_interface = NetworkInterface();
                continue;
            }
        }
    }

    return interfaces;
}



void test_get_data_from_ifconfig_data() {
    printf("lol\n");
    std::string data = R"(
lo        Link encap:UNSPEC
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope: Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:48 errors:0 dropped:0 overruns:0 frame:0
          TX packets:48 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:5376 TX bytes:5376

dummy0    Link encap:UNSPEC
          inet6 addr: fe80::d467:beff:fefe:d3c2/64 Scope: Link
          UP BROADCAST RUNNING NOARP  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:12 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:0 TX bytes:840

wlan0     Link encap:UNSPEC    Driver cnss_pci
          inet addr:192.168.178.23  Bcast:192.168.178.255  Mask:255.255.255.0
          inet6 addr: fe80::c65f:a766:eb9f:c2b3/64 Scope: Link
          inet6 addr: 2a02:1368:6200:2d0c:f987:cc24:7873:6f05/64 Scope: Global
          inet6 addr: 2a02:1368:6200:2d0c:be86:8a48:8ea4:4f0e/64 Scope: Global
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:1630171 errors:0 dropped:0 overruns:0 frame:0
          TX packets:328820 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:3000
          RX bytes:2347163696 TX bytes:37245802
)";

    auto interfaces = get_data_from_ifconfig_data(data);
    printf("interfaces: %zu\n", interfaces.size());

    for(auto &i: interfaces) {
        printf("Interface. name:%s, address:%s\n", i.name.c_str(), i.inet.c_str());
    }
}

bool variant_matches_type(godot::Variant variant, godot::Variant::Type expected_type, bool log)
{
    auto actual_type = variant.get_type();
    if (actual_type == expected_type){
        return true;
    }

    auto expected_type_name = variant.get_type_name(expected_type);
    auto actual_type_name = variant.get_type_name(actual_type);

    if (log) {
        LOG(tfm::format(
            "Type of variant should be %s, instead is %s\n",
            expected_type_name.utf8().get_data(),
            actual_type_name.utf8().get_data()
        ));
    }
    
    return false;
}

std::string str(godot::String string)
{
    return std::string(string.utf8().get_data());
}

std::string str(godot::StringName string_name)
{
    return str(godot::String(string_name));
}

std::string str(godot::Dictionary dictionary)
{
    return str(godot::UtilityFunctions::str(dictionary));
}

std::string str(godot::Vector3 vector)
{
    return tfm::format("Vec3(%f, %f, %f)", vector.x, vector.y, vector.z);
}

std::string str(godot::Vector2 vector)
{
    return tfm::format("Vec2(%f, %f)", vector.x, vector.y);
}
