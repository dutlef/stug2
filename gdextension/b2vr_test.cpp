#include "b2vr_test.h"
#include <godot_cpp/core/class_db.hpp>
#include <stdio.h>


using namespace godot;

void B2VRTest::_bind_methods() {
    ClassDB::bind_method(D_METHOD("get_float"), &B2VRTest::get_float);
}


B2VRTest::B2VRTest() {
	time_passed = 0.0;

}

B2VRTest::~B2VRTest() {
}

void B2VRTest::_process(double delta) {
    // Godot::print("Logging to console in GDExtension");
	time_passed += delta;
}

float B2VRTest::get_float() {
    return sin(time_passed);
}