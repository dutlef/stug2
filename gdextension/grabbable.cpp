#include "grabbable.h"
#include "util.h"

#include <godot_cpp/classes/capsule_mesh.hpp>
#include <godot_cpp/classes/mesh.hpp>


using namespace godot;

void Grabbable::_bind_methods() {
}

Grabbable::Grabbable() {
}

Grabbable::~Grabbable() {
}

void Grabbable::_ready() {
    left_hand = get_node<Hand>("/root/Simple/SceneRoot/XR_Origin/Left_Controller/Left_Hand");
    right_hand = get_node<Hand>("/root/Simple/SceneRoot/XR_Origin/Right_Controller/Right_Hand");
    root = get_node<SceneRoot>("/root/Simple/SceneRoot");
    initial_parent = get_parent_node_3d()->get_parent_node_3d();
    initial_scale = mesh_instance->get_scale();

    if (left_hand == nullptr) {
        LOG("left hand not found");
        queue_free();
    }

    if (right_hand == nullptr) {
        LOG("left hand not found");
        queue_free();
    }

    // mesh_instance->set_material_overlay(root->grabbable_overlay);
    LOG("Grabbable: %s", String(mesh_instance->get_name()).utf8().get_data());

    auto aabb = mesh_instance->get_mesh()->get_aabb();
    auto size = aabb.get_size() * mesh_instance->get_scale();
    float diagonal = Math::sqrt(
        Math::pow(size.x, 2.0f) + Math::pow(size.y, 2.0f) + Math::pow(size.z, 2.0f)
    );
    threshold_squared = Math::pow(diagonal / 2.0f, 2.0f);
}

void Grabbable::set_mesh_instance(MeshInstance3D* mesh_instance)
{
    this->mesh_instance = mesh_instance;
}

bool Grabbable::get_being_held()
{
    return being_held_left || being_held_right;
}

void Grabbable::_process(double delta) {

    auto left_dst_squared = left_hand->get_global_position().distance_squared_to(get_global_position());
    auto right_dst_squared = right_hand->get_global_position().distance_squared_to(get_global_position());

    if (left_dst_squared < threshold_squared && !left_close) {
        left_close = true;
        left_hand->register_grabbable(this, initial_parent);
    }

    if (left_dst_squared >= threshold_squared && left_close) {
        left_close = false;
        left_hand->unregister_grabbable(this);
    }

    if (right_dst_squared < threshold_squared && !right_close) {
        right_close = true;
        right_hand->register_grabbable(this, initial_parent);
    }

    if (right_dst_squared >= threshold_squared && right_close) {
        right_close = false;
        right_hand->unregister_grabbable(this);
    }

    // LOG("left_close  %f\nright_close %f \nthreshold%f", left_dst_squared, right_dst_squared, threshold_squared);

    if (left_close || right_close) {
        if (!any_hand_within_distance) {
            any_hand_within_distance = true;
            // mesh_instance->set_scale(initial_scale * Vector3(1.0, 4.0, 1.0));
            LOG("now close");
            // mesh_instance->set_material_overlay(root->grabbable_overlay);
        }
    } else {    
        if (any_hand_within_distance) {
            // mesh_instance->set_scale(initial_scale);
            any_hand_within_distance = false;
            LOG("now far");
            // mesh_instance->set_material_overlay(nullptr);
        }
    }
}

