#pragma once

#include "hand.h"
#include "scene_root.h"
#include <godot_cpp/classes/node3d.hpp>
#include <godot_cpp/classes/xr_origin3d.hpp>
#include <godot_cpp/classes/xr_controller3d.hpp>
#include <godot_cpp/classes/xr_camera3d.hpp>
#include <godot_cpp/classes/mesh_instance3d.hpp>

namespace godot {

class HasInitialParent : public Node3D {
	GDCLASS(HasInitialParent, Node3D)

private:
	Node3D* initial_parent;

protected:
	static void _bind_methods();

public:
	HasInitialParent();
	~HasInitialParent();

	void _process(double delta) override;
	void _ready() override;
};
}

