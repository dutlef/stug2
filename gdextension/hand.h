#pragma once

#include "command_processor.h"

#include <godot_cpp/classes/node3d.hpp>
#include <godot_cpp/classes/xr_origin3d.hpp>
#include <godot_cpp/classes/xr_controller3d.hpp>
#include <godot_cpp/classes/xr_camera3d.hpp>
#include <godot_cpp/classes/standard_material3d.hpp>
#include <godot_cpp/classes/xr_camera3d.hpp>
#include <godot_cpp/classes/mesh_instance3d.hpp>

namespace godot {

class Hand : public Node3D {
	GDCLASS(Hand, Node3D)

private:
    XRController3D* controller;
    MeshInstance3D* mesh_instance;
    XRCamera3D* camera;
	std::map<uint64_t, std::pair<Node3D*, Node3D*>> close_grabbables = {};
	bool already_grabbing;
	int held_objects = 0;
	Hand* other_hand;

	void on_grab();
	void on_grab_stop();

	static const int num_velocities = 10;
	Vector3 velocities[num_velocities];
	int velocities_index = 0;
	Vector3 previous_position;

	Vector3 get_interpolated_velocity();

	MeshInstance3D* dir_marker;
	MeshInstance3D* curve_cursor;

	StandardMaterial3D* point_material = memnew(StandardMaterial3D);
	Color curve_invalid_color = Color(1.0, 0.0, 0.0);
	Color curve_valid_color = Color(0.0, 0.0, 1.0);

	static const int curve_iterations = 50;
	float curve_velocity = 5.f;

	std::array<Node3D*, curve_iterations> curve_points = {};

	void calculate_curve();
	bool find_curve_hit_point();
	void track_velocity(double delta);

	bool already_pushing_forward = false;
	void on_push_forward();
	void on_push_forward_stop();

	Node3D* points_parent = memnew(Node3D);

	bool valid_target;

protected:
	static void _bind_methods();

public:
	Hand();
	~Hand();


	void set_other_hand(Hand* other_hand);
	void set_camera(XRCamera3D* camera);
	void release(uint64_t id);
	void release(std::pair<Node3D*, Node3D*> pair);

	void _process(double delta) override;
	void _physics_process(double delta) override;
	void _ready() override;

	void set_controller(XRController3D* controller);

	void register_grabbable(Node3D* grabbable, Node3D* parent);
	void unregister_grabbable(Node3D* grabbable);
};
}

