#pragma once

#include "command_processor.h"
#include <godot_cpp/classes/node3d.hpp>
#include <godot_cpp/classes/xr_origin3d.hpp>
#include <godot_cpp/classes/xr_controller3d.hpp>
#include <godot_cpp/classes/xr_camera3d.hpp>

namespace godot {

class CustomXROrigin : public XROrigin3D {
	GDCLASS(CustomXROrigin, XROrigin3D)

private:
    XRController3D* left_controller;
    XRController3D* right_controller;
    XRCamera3D* camera;

	String get_ip_address();

protected:
	static void _bind_methods();

public:
	CustomXROrigin();
	~CustomXROrigin();

	void _process(double delta) override;
	void _ready() override;

};
}

