#ifndef B2VRTest_H
#define B2VRTest_H

#include <godot_cpp/classes/node3d.hpp>

namespace godot {

class B2VRTest : public Node {
	GDCLASS(B2VRTest, Node)

private:
	double time_passed;

protected:
	static void _bind_methods();

public:
	B2VRTest();
	~B2VRTest();

	void _process(double delta) override;

    float get_float();
};

}

#endif
