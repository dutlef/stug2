#pragma once

#include <godot_cpp/classes/gltf_document_extension.hpp>
#include <godot_cpp/classes/node3d.hpp>




namespace godot {

class CustomGLTFExtension : public GLTFDocumentExtension {

public:
	// virtual Node3D* _generate_scene_node(const Ref<GLTFState> &state, const Ref<GLTFNode> &gltf_node, Node *scene_parent) override;
	// virtual Error _import_node(const Ref<GLTFState> &state, const Ref<GLTFNode> &gltf_node, const Dictionary &json, Node *node);
    // Error _import_post(Ref<GLTFState> p_state, Node *p_root) override;

    // Error import_post(Ref<GLTFState> p_state, Node *p_root) override;
	Error _import_post(const Ref<GLTFState> &state, Node *root) override;


protected:
	static void _bind_methods();

};


}
