#include "custom_xr_origin.h"
#include "hand.h"
#include "util.h"
#include "command_processor.h"
#include <godot_cpp/core/class_db.hpp>
#include <stdio.h>
#include <godot_cpp/classes/node.hpp>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include <godot_cpp/classes/os.hpp>
#include <godot_cpp/classes/file_access.hpp>
#include <godot_cpp/classes/resource_saver.hpp>
#include <godot_cpp/classes/file_dialog.hpp>
#include <godot_cpp/classes/file_system_dock.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/variant/string.hpp>
#include <godot_cpp/classes/xr_server.hpp>
#include <godot_cpp/classes/xr_interface.hpp>
#include <godot_cpp/classes/display_server.hpp>
#include <godot_cpp/classes/ip.hpp>
#include <godot_cpp/classes/text_mesh.hpp>

using namespace godot;

void CustomXROrigin::_bind_methods() {
}

CustomXROrigin::CustomXROrigin() {
}

CustomXROrigin::~CustomXROrigin() {
}

void CustomXROrigin::_ready() {

    LOG("XR ORIGIN");

    #ifdef XR
    LOG("We are on XR :DDD");
    #endif

    left_controller = memnew(XRController3D);
    left_controller->set_name("Left_Controller");
    right_controller = memnew(XRController3D);
    right_controller->set_name("Right_Controller");

    camera = memnew(XRCamera3D);
    camera->set_name("XR_Camera");
    camera->make_current();

    left_controller->set_tracker("left_hand");
    right_controller->set_tracker("right_hand");

    add_child(left_controller);
    add_child(right_controller);
    add_child(camera);
    LOG("camera path %s", str(camera->get_path()));

    auto left_hand = memnew(Hand);
    left_hand->set_name("Left_Hand");
    left_hand->set_controller(left_controller);
    left_hand->set_camera(camera);
    left_controller->add_child(left_hand);

    auto right_hand = memnew(Hand);
    right_hand->set_name("Right_Hand");
    right_hand->set_controller(right_controller);
    right_hand->set_camera(camera);
    right_controller->add_child(right_hand);

    right_hand->set_other_hand(left_hand);
    left_hand->set_other_hand(right_hand);


    auto ip_address_mesh_instance = memnew(MeshInstance3D);
    auto text_mesh = memnew(TextMesh);
    text_mesh->set_text(get_ip_address());
    text_mesh->set_depth(0);
    ip_address_mesh_instance->set_mesh(text_mesh);
    add_child(ip_address_mesh_instance);
    ip_address_mesh_instance->set_position(Vector3(0, 1.5, -3));

    auto xr_interface = XRServer::get_singleton()->find_interface("OpenXR");

    if (xr_interface != nullptr && xr_interface.ptr()->is_initialized()) {
        DisplayServer::get_singleton()->window_set_vsync_mode(DisplayServer::VSYNC_DISABLED);
        get_viewport()->set_use_xr(true);
    } else {
        LOG("OpenXR not initialized");
    }
    
}

String CustomXROrigin::get_ip_address()
{
    LOG("Adresses:");
    auto addresses = IP::get_singleton()->get_local_addresses();
    auto good_address = String("");
    for (int i = 0; i < addresses.size(); i++)
    {
        auto address = addresses[i];
        bool is_ipv6 = address.contains(":");
        bool is_local = address.find("127.") == 0;
        bool valid_address = !is_ipv6 && !is_local;
        LOG("   %s", str(address));
        if (valid_address) {
            good_address = address;
        }
    }

    if (good_address.length() < 1) {
        good_address = String("No IP address found.");
        LOG("Error: no good IP address found");
    }

    return good_address;
}

void CustomXROrigin::_process(double delta) {
}

