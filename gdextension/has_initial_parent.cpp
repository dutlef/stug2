#include "has_initial_parent.h"
#include "util.h"



using namespace godot;

void HasInitialParent::_bind_methods() {}

HasInitialParent::HasInitialParent() {}

HasInitialParent::~HasInitialParent() {}

void HasInitialParent::_ready() 
{
    initial_parent = get_parent_node_3d()->get_parent_node_3d();
}

void HasInitialParent::_process(double delta) {}

