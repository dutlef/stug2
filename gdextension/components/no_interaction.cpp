
#include "no_interaction.h"
#include "util.h"

#include <godot_cpp/classes/node3d.hpp>

using namespace godot;

// todo: send udp packet to broadcast address.

void NoInteraction::_bind_methods() {
}

NoInteraction::NoInteraction() {
}

NoInteraction::~NoInteraction() {
}

void NoInteraction::enable()
{
    InteractionNode::enable();
}

void NoInteraction::disable()
{
    InteractionNode::disable();
}
