#pragma once 

#include "_interaction_node.h"

#include <godot_cpp/classes/node3d.hpp>

#include <vector>
#include <array>
#include <map>

namespace godot {

struct DynamicObjectSettings {
	std::array<float, 3> color = {1.0, 1.0, 1.0}; 
	String texture_path = "";
    bool grabbable;
    bool remote_grab;
    bool is_collider;
	bool is_mesh;
	bool is_nav_mesh;
	bool use_gravity;

	std::map<std::string, InteractionNode*> interactions = {};
};


class DynamicObject : public Node3D {
	GDCLASS(DynamicObject, Node3D)

protected:
	static void _bind_methods();

public:
	DynamicObject();
	~DynamicObject();

    DynamicObjectSettings* settings = new DynamicObjectSettings();

	void _ready() final;
	void _process(double delta) final;

	void trigger_interaction_start();
};
}