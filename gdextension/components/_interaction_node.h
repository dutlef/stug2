#pragma once 

#include <godot_cpp/classes/node3d.hpp>

namespace godot {

class InteractionNode : public Node3D {
	GDCLASS(InteractionNode, Node3D)

    bool enabled = false;
    bool ready = false;
    float smooth_enabled = 0.f;

protected:
	static void _bind_methods();
	Node3D* controlled_object;
    bool is_enabled();
    float get_smooth_enabled();
    Node3D* gltf_root;
    Node3D* xr_camera;
    Node3D* trigger_object;

public:
	InteractionNode();
	~InteractionNode();

    virtual void enable();
    virtual void disable();
	void _ready() override;
	void _process(double delta) override;

    float transition_duration = 0.5f;
    float extent = 0.5f;
    float speed = 1.0f;
	std::array<float, 3> new_color = {1.0, 1.0, 1.0}; 
    int interaction_id = 0;

    int object_type_index = -1;
    String object_name = "";

    String string_a = "";

    std::string trigger = "unset";

    void set_gltf_root(Node3D* node);
    Node* find_node_by_name(Node* current_node);

	void dynamic_start();

};
}