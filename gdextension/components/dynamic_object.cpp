#include "../util.h"
#include "dynamic_object.h"
#include "float_in_space.h"


using namespace godot;

void DynamicObject::_bind_methods()
{
}

DynamicObject::DynamicObject()
{
}

DynamicObject::~DynamicObject()
{
}

void DynamicObject::_ready()
{
	
}

void DynamicObject::trigger_interaction_start()
{
	LOG("(%s) has the following interactions\n", str(get_name()));

	for (auto const& x : settings->interactions)
	{
		auto trigger = x.first;
		auto interaction_node = x.second;
		auto interaction_name = String(interaction_node->get_name()).utf8().get_data();
		LOG("   %s => %s", trigger, interaction_name);
		add_child(interaction_node);
		LOG("======== /dynamic-start\n");
		interaction_node->dynamic_start();
		LOG("======== dynamic-start/\n");

		// if (x.first == std::string("b2vr_on_begin")) {
		// 	interaction_node->enable();
		// }
	}
	// LOG("(%s) trigger_interaction_start", str(get_name()));
	LOG("========================================================\n\n");
}

void DynamicObject::_process(double delta)
{

}