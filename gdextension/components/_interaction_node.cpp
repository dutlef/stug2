
#include "_interaction_node.h"
#include "util.h"

#include <godot_cpp/classes/node3d.hpp>


using namespace godot;

void InteractionNode::enable()
{
	enabled = true;
}

void InteractionNode::disable()
{
	enabled = false;
}

void InteractionNode::_bind_methods()
{
}

bool InteractionNode::is_enabled()
{
	return enabled;
}

float InteractionNode::get_smooth_enabled()
{
	return smooth_enabled;
}


godot::InteractionNode::InteractionNode()
{
}

godot::InteractionNode::~InteractionNode()
{
}

void InteractionNode::_ready()
{
	controlled_object = this->get_parent_node_3d()->get_parent_node_3d();
}

void InteractionNode::dynamic_start() {
	LOG("--- %f", 	is_inside_tree());

	if (!is_inside_tree()) {
		LOG("Skipped object");
		return;
	}

	LOG("(InteractionNode) %s, trigger %s", str(controlled_object->get_name()), trigger);

	if (trigger == std::string("b2vr_on_begin"))
	{
		LOG("enabling on begin (%s)", str(controlled_object->get_name()));
		enable();
	}


	if (trigger == std::string("b2vr_on_proximity_enter"))
	{
		LOG("  object_type_index %i", object_type_index);
		auto path = NodePath("/root/Simple/SceneRoot/XR_Origin/XR_Camera");
		trigger_object = get_node<Node3D>(path);
		LOG("  Set XR Camera as trigger object");
		
		if (object_type_index == 1) {
			auto node = find_node_by_name(gltf_root);

			if (node == nullptr)
			{
				LOG("  Could not set node by name '%s', because it is a nullptr", str(object_name));
			}
			else
			{
				LOG("  Set '%s' as trigger object", str(object_name));
				LOG("  Node name '%s'", str(node->get_name()));
				trigger_object = Object::cast_to<Node3D>(node);
				LOG("  Node name '%s'", str(trigger_object->get_name()));
			}
		}
	}

	ready = true;
	LOG("///");
}

void InteractionNode::_process(double delta)
{
	smooth_enabled = Math::lerp(smooth_enabled, (float) enabled, (float) delta);
	if (!ready)
	{
		LOG("not ready!!!!");
		return;
	}

	if (trigger == std::string("b2vr_on_proximity_enter"))
	{
		double dist_sq = controlled_object->get_global_position().distance_squared_to(trigger_object->get_global_position());

		// if (String(controlled_object->get_name()) == String("GrayBall")) 
		// {
		// 	LOG("dist sqrd %f", dist_sq);
		// }

		if (dist_sq < 4.0 && !enabled)
		{
			enable();
		}

		if (dist_sq >= 4 && enabled)
		{
			disable();
		}
	}
}

void InteractionNode::set_gltf_root(Node3D *node)
{
	gltf_root = node;
}

Node* InteractionNode::find_node_by_name(Node* current_node)
{
	for (int i = 0; i < current_node->get_child_count(); i++)
	{
		auto child = current_node->get_child(i);

		
		bool normal_object = String(child->get_name()) == object_name && !child->is_queued_for_deletion();
		bool gravity_object = String(child->get_name()) == String("RB_") + object_name;
		if (gravity_object || normal_object)
		{
			return child;
		}
		auto found = find_node_by_name(child);

		if (found != nullptr)
		{
			LOG("OK: finally found a child with that name: '%s'", str(object_name));
			LOG("OK: '%s'", str(found->get_name()));
        	LOG("OK: %s %i", str(found->get_name()), found->get_instance_id());

			return found;
		}
	}

		
	return nullptr;
}
