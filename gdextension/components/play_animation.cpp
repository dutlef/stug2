
#include "play_animation.h"
#include "util.h"

#include <godot_cpp/classes/node3d.hpp>
#include <godot_cpp/classes/animation.hpp>

using namespace godot;

void PlayAnimation::_bind_methods() {
    LOG("loll");
}

PlayAnimation::PlayAnimation() {
}

PlayAnimation::~PlayAnimation() {
}

void PlayAnimation::_ready() {

    InteractionNode::_ready();

    if (player == nullptr)
    {
        set_player();
    }
}

void PlayAnimation::enable()
{
    InteractionNode::enable();

    if (player == nullptr)
    {
        set_player();
    }

    LOG("pplay %s on %s", str(string_a), str(controlled_object->get_name()));
    if (player == nullptr) {
        LOG("Player is nullptr");
    }
    player->play(string_a);
    LOG("playing");
}

void PlayAnimation::disable()
{
    InteractionNode::disable();
}

void PlayAnimation::_process(double delta)
{
    InteractionNode::_process(delta);
}

void PlayAnimation::set_player()
{
    auto path = NodePath("scene/AnimationPlayer");
    auto original = gltf_root->get_node<AnimationPlayer>(path);
    player = Object::cast_to<AnimationPlayer>(original->duplicate());
    
    auto animation = player->get_animation(string_a);
    if (animation != nullptr) {
        // animation.ptr()->set_loop_mode(Animation::LOOP_LINEAR);
    }
    original->get_parent()->add_child(player);
    LOG("set pplayer %s on %s", str(string_a), str(controlled_object->get_name()));
}