#pragma once 

#include <godot_cpp/classes/node3d.hpp>
#include <godot_cpp/classes/animation_player.hpp>
#include "_interaction_node.h"

namespace godot {

class PlayAnimation : public InteractionNode {
	GDCLASS(PlayAnimation, InteractionNode)

private:
	AnimationPlayer* player;
	String animation_name = "";
    AnimationPlayer* root;

	void set_player();

protected:
	static void _bind_methods();

public:
	PlayAnimation();
	~PlayAnimation();

	void enable() override;
	void disable() override;

	void _process(double delta) override;
	void _ready() override;
};
}