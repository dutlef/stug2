#pragma once 

#include <godot_cpp/classes/node3d.hpp>
#include "_interaction_node.h"

namespace godot {

class FloatInSpace : public InteractionNode {
	GDCLASS(FloatInSpace, InteractionNode)

protected:
	static void _bind_methods();

public:
	FloatInSpace();
	~FloatInSpace();

	Vector3 initial_position;
	double time = 0.0;

	void enable() final;
	void disable() final;

	void _process(double delta) override;
	void _ready() override;

	void set_extent(float extent);
	void set_speed(float speed);
};
}