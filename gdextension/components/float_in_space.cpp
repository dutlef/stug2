
#include "float_in_space.h"
#include "util.h"

#include <godot_cpp/classes/node3d.hpp>

using namespace godot;

// todo: send udp packet to broadcast address.

void FloatInSpace::_bind_methods() {
    LOG("loll");
}

FloatInSpace::FloatInSpace() {
}

FloatInSpace::~FloatInSpace() {
}

void FloatInSpace::_ready() {
    InteractionNode::_ready();
    LOG("its me, floating :)) extent %f", extent);
    initial_position = controlled_object->get_position();
    LOG("controlled_object %s", controlled_object->get_name().to_snake_case().utf8().get_data());    
}

void FloatInSpace::set_extent(float extent)
{
    this->extent = extent;
}

void FloatInSpace::set_speed(float speed)
{
    this->speed = speed;
}

void FloatInSpace::enable()
{
    InteractionNode::enable();
    LOG("%s is soon floating (%f, %f)", str(controlled_object->get_name()), extent, speed);
}

void FloatInSpace::disable()
{
    InteractionNode::disable();
}

void FloatInSpace::_process(double delta)
{
    InteractionNode::_process(delta);
    time += delta;

    // need to do this check, because other reactions might change the position as well
    if (get_smooth_enabled() > 0.f)
    {
        float y = extent * Math::sin(time * speed);
        controlled_object->set_position(initial_position + Vector3(0.0, Math::lerp(0.f, y, get_smooth_enabled()), 0.0));
    }
}
