#pragma once 

#include <godot_cpp/classes/node3d.hpp>
#include "_interaction_node.h"

namespace godot {

class NoInteraction : public InteractionNode {
	GDCLASS(NoInteraction, InteractionNode)

protected:
	static void _bind_methods();

public:
	NoInteraction();
	~NoInteraction();

	void enable() final;
	void disable() final;
};
}