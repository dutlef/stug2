#include "hand.h"
#include "util.h"

#include <godot_cpp/classes/capsule_mesh.hpp>
#include <godot_cpp/classes/cylinder_mesh.hpp>
#include <godot_cpp/classes/box_mesh.hpp>
#include <godot_cpp/classes/sphere_mesh.hpp>
#include <godot_cpp/classes/rigid_body3d.hpp>
#include <godot_cpp/classes/engine.hpp>
#include <godot_cpp/classes/world3d.hpp>
#include <godot_cpp/classes/physics_direct_space_state3d.hpp>
#include <godot_cpp/classes/physics_ray_query_parameters3d.hpp>
#include <godot_cpp/classes/standard_material3d.hpp>
#include <godot_cpp/variant/basis.hpp>
#include <godot_cpp/core/math.hpp>


using namespace godot;


void Hand::_bind_methods()
{
}

Hand::Hand() {
}

Hand::~Hand() {
}

void Hand::_ready() {
    if (Engine::get_singleton()->is_editor_hint()) {
        set_process_mode(Node::ProcessMode::PROCESS_MODE_DISABLED);
    }


    Ref<CylinderMesh> cylinder = memnew(CylinderMesh);
    cylinder->set_top_radius(0);
    cylinder->set_bottom_radius(0.06);
    cylinder->set_height(0.12);

    mesh_instance = memnew(MeshInstance3D);
    mesh_instance->set_name("Hand Mesh");
    mesh_instance->rotate(Vector3(1, 0, 0), Math::deg_to_rad(-45.0));
    mesh_instance->set_name("Controller Mesh");
    mesh_instance->set_mesh(cylinder);

    Ref<CapsuleMesh> capsule = memnew(CapsuleMesh);
    capsule->set_radius(0.02);
    capsule->set_height(0.4);

    dir_marker = memnew(MeshInstance3D);
    dir_marker->set_name("Dir Marker");
    dir_marker->set_mesh(capsule);
    dir_marker->set_position(Vector3(0, 1, 0));
    mesh_instance->add_child(dir_marker);

    curve_cursor = memnew(MeshInstance3D);
    curve_cursor->set_name("Curve Cursor");
    curve_cursor->set_mesh(capsule);
    curve_cursor->set_as_top_level(true);
    add_child(curve_cursor);

    add_child(mesh_instance);

    auto curve_point_mesh = memnew(SphereMesh);
    curve_point_mesh->set_radius(0.025);
    curve_point_mesh->set_height(0.05);
    point_material->set_albedo(curve_invalid_color);
    curve_point_mesh->set_material(point_material);

    points_parent->set_as_top_level(true);
    add_child(points_parent);

    for (int i = 0; i < curve_iterations; i++)
    {
        auto point = memnew(MeshInstance3D);
        point->set_name("Curve Point");
        point->set_mesh(curve_point_mesh);
        points_parent->add_child(point);
        curve_points[i] = point;
    }
    
}


void Hand::calculate_curve()
{
    // wtf where is PI? cannot find it in godot::Math
    float PI = 3.14159265358979323846f;

    Vector3 to = -mesh_instance->get_global_basis()[1];
    float angle_radians =  Vector3(0, 1, 0).signed_angle_to(
        to, Vector3(0, 1, 0)
    ) - PI / 2.f;

	float g = 9.81f;
	float y_rot = mesh_instance->get_global_rotation().y;

	Vector3 forward = dir_marker->get_global_position() - mesh_instance->get_global_position();

	Vector3 pos = mesh_instance->get_global_position() + forward;
	dir_marker->set_global_position(pos);

	Vector3 flat_dir = (forward * Vector3(1.0, 0.0, 1.0)).normalized();
	
    for (int i = 0; i < curve_iterations; i++)
    {
        float t = i * 0.05f;
		float a = curve_velocity * Math::cos(angle_radians) * t;
		float b = curve_velocity * Math::sin(angle_radians) * t - 0.5 * g * Math::pow(t, 2.0f);
        
		curve_points[i]->set_global_position(
            mesh_instance->get_global_position() + Vector3(flat_dir.x * a, b, flat_dir.z * a)
        );
    }
}

void Hand::set_controller(XRController3D *controller)
{
    this->controller = controller;
}


void Hand::register_grabbable(Node3D* grabbable, Node3D* parent)
{
    auto object = grabbable->get_parent_node_3d();
    close_grabbables.insert({grabbable->get_instance_id(), {object, parent}});
}

void Hand::unregister_grabbable(Node3D *grabbable)
{
    close_grabbables.erase(grabbable->get_instance_id());
}

void Hand::_process(double delta)
{
    if (controller->is_button_pressed("grip_click"))
    {
        if (!already_grabbing) 
        {
            on_grab();
        }
        already_grabbing = true;
        
    }
    else
    {
        if (already_grabbing)
        {
            on_grab_stop();
        }
        already_grabbing = false;
    }

    auto stick = controller->get_vector2("primary");

    if (stick.y > 0.6)
    {
        if (!already_pushing_forward)
        {
            on_push_forward();
        }
        already_pushing_forward = true;
    }
    else
    {
        if (already_pushing_forward)
        {
            on_push_forward_stop();
        }
        already_pushing_forward = false;
    }

    // auto grab = controller->get_float("grab");
}

void Hand::on_push_forward()
{
    calculate_curve();
    find_curve_hit_point();
    points_parent->set_visible(true);
}

void Hand::on_push_forward_stop()
{
    if (!valid_target)
    {
        return;
    }

    LOG("Teleport");
    points_parent->set_visible(false);
    auto origin = get_parent_node_3d()->get_parent_node_3d();
    auto diff = (origin->get_global_position() - camera->get_global_position()) * Vector3(1, 0, 1);
    origin->set_global_position(curve_cursor->get_global_position() + diff);
}


void Hand::_physics_process(double delta)
{
    track_velocity(delta);

    if (already_pushing_forward)
    {
        calculate_curve();
        valid_target = find_curve_hit_point();
        if (valid_target)
        {
            point_material->set_albedo(curve_valid_color);
        }
        else
        {
            point_material->set_albedo(curve_invalid_color);
        }
    }
}

void Hand::track_velocity(double delta)
{
 // auto v = (get_global_position());
    auto velocity = (get_global_position() - previous_position) / delta;
    // LOG("added (%f, %f, %f)", v.x, v.y, v.z);
    velocities[velocities_index] = velocity;
    previous_position = get_global_position();

    velocities_index = (velocities_index + 1) % num_velocities;
}

bool Hand::find_curve_hit_point()
{
    auto space_state = get_world_3d().ptr()->get_direct_space_state();

    for (int i = 0; i < curve_iterations - 1; i++)
    {
        auto origin = curve_points[i]->get_global_position();
		auto end = curve_points[i + 1]->get_global_position();

		auto query = PhysicsRayQueryParameters3D::create(origin, end);
		query->set_collide_with_areas(true);
        query->set_collision_mask(NAV_MESH_LAYER);
		auto result = space_state->intersect_ray(query);

		if (!result.is_empty()) 
        {
            Vector3 normal = result.get("normal", Vector3(0, 0, 0));
            Vector3 position = result.get("position", Vector3(0, 0, 0));
            curve_cursor->set_quaternion(Quaternion(Vector3(0, 1, 0), normal));
			curve_cursor->set_position(position);
			return true;
        }
    }

    return false;
}


Vector3 Hand::get_interpolated_velocity() {
    Vector3 vec = Vector3();
    float influences_sum = 0.0f;

    for (int i = 0; i < num_velocities; i++)
    {
        float influence = (i + 1) / num_velocities;
        vec += velocities[(velocities_index + i) % num_velocities] * influence;
        influences_sum += influence;
    }
    LOG("influcense %f", influences_sum);
    vec /= influences_sum;

    return vec;
}

void Hand::on_grab()
{
    LOG("On Grab");
    for (auto const& [id, val] : close_grabbables)
    {
        const auto& [object, initial_parent] = val;
        other_hand->release(id);

        LOG(
            "reparenting %s from %s to %s",
            str(object->get_name()),
            str(object->get_parent_node_3d()->get_name()),
            str(this->get_name())
        );
        object->reparent(this);

        LOG("class is %s", str(object->get_class()));
        if (object->is_class("RigidBody3D")) {
            LOG("object is Rigidboy");
            auto rb = Object::cast_to<RigidBody3D>(object);
            rb->set_freeze_enabled(true);
        }

        held_objects++;
    }

    if (held_objects > 0) {
        mesh_instance->show();
    }
}

void Hand::on_grab_stop()
{
    LOG("On Grab Stop");
    for (auto const& [id, val] : close_grabbables)
    {
       release(val);
    }
}

void Hand::release(std::pair<godot::Node3D *, godot::Node3D *> pair)
{
    const auto& [object, initial_parent] = pair;

    LOG(
        "reparenting %s from %s to %s",
        str(object->get_name()),
        str(object->get_parent_node_3d()->get_name()),
        str(initial_parent->get_name())
    );
    object->reparent(initial_parent);

    if (object->is_class("RigidBody3D")) {
        LOG("object is Rigidboy");
        auto rb = Object::cast_to<RigidBody3D>(object);
        rb->set_freeze_enabled(false);
        // TODO: inherit from RigidBody3D and override _integrate_forces()
        auto v = get_interpolated_velocity();
        LOG("interpolated velocity: (%f, %f, %f)", v.x, v.y, v.z);
        rb->set_linear_velocity(v);
        // rb->add_constant_force(Vector3(0.0, 1.0, 0.0));
    }

    held_objects--;

    if (held_objects == 0) {
        mesh_instance->show();
    }
}

void Hand::set_other_hand(Hand* other_hand)
{
    this->other_hand = other_hand;
}

void Hand::set_camera(XRCamera3D* camera)
{
    this->camera = camera;
}


void Hand::release(uint64_t id) 
{
    if (close_grabbables.count(id) == 0) {
        LOG("could not find a grabbable to release");
        return;
    }

    release(close_grabbables[id]);
}