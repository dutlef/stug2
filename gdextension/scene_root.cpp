#include "scene_root.h"
#include "util.h"
#include "command_processor.h"
#include <godot_cpp/core/class_db.hpp>
#include <stdio.h>
#include <godot_cpp/classes/node.hpp>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <godot_cpp/classes/os.hpp>

#include "tinyformat/tinyformat.h"

#include <godot_cpp/classes/os.hpp>
#include <godot_cpp/classes/file_access.hpp>
#include <godot_cpp/classes/resource_saver.hpp>
#include <godot_cpp/classes/file_dialog.hpp>
#include <godot_cpp/classes/file_system_dock.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/variant/string.hpp>

#include "custom_xr_origin.h"

// #include <godot_cpp/core/os.hpp>

// #ifdef ANDROID
// #include <android/log.h>
// #endif
// #include <exception>      // std::exception, std::terminate

using namespace godot;

// todo: send udp packet to broadcast address.

void SceneRoot::_bind_methods() {
}

SceneRoot::SceneRoot() {
    command_processor = memnew(CmdProcessor);
    grabbable_overlay = memnew(StandardMaterial3D);
    grabbable_overlay->set_albedo(Color(0.3f, 0.3f, 0.3f));
    grabbable_overlay->set_blend_mode(BaseMaterial3D::BlendMode::BLEND_MODE_ADD);
}

SceneRoot::~SceneRoot() {
}

void SceneRoot::_ready() {
    // auto file = godot::FileAccess::open("user://xddddd.dat", FileAccess::WRITE);
    // file->store_line("lolllllll miau");
    // file->close();

    auto xr_origin = memnew(CustomXROrigin);
    xr_origin->set_name("XR_Origin");
    add_child(xr_origin);


    auto udp = memnew(PacketPeerUDP);
    udp->set_broadcast_enabled(true);
    // udp->connect_to_host("10.155.127.255", 12345);
    udp->connect_to_host("10.155.119.201", 12345);
    udp->put_packet(String("The answer is... 42!").to_utf8_buffer());

    // LOG(":::::");
    // get_local_address();
    // LOG("__");
    // get_broadcast_addr();
    // LOG(":::::");

    //
    // nc -ul 12345
    //

    // OS os;
    // // String dir = os.get_user_data_dir();
    // // printf("Dir %s\n", dir.utf8().get_data());


    // #ifdef ANDROID
    // // __android_log_print(ANDROID_LOG_WARN, "LOG_TAG", "miauuu");
    // // std::terminate();
    // // exit(0);
    // #endif

    // auto args = PackedStringArray();
    // auto output = Array();
    // os.execute("ifconfig", args, output, true);

    // LOG(tfm::format("length %i\n", output.size()));
    // auto first = output[0];
    // String stringified = first.stringify();

    // LOG(tfm::format("str===\n %s\n===\n", stringified.utf8().get_data()));

    // ///////////

    add_child(command_processor);

    // test_get_data_from_ifconfig_data();
}

void SceneRoot::_process(double delta) {
    // printf("ahaha ");
    // Godot::print("Logging to console in GDExtension");
}

