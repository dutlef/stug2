#ifndef COMMAND_PROCESSOR_H
#define COMMAND_PROCESSOR_H

#include <godot_cpp/classes/node.hpp>
#include <godot_cpp/classes/udp_server.hpp>
#include <godot_cpp/classes/packet_peer_udp.hpp>
#include <godot_cpp/classes/world_environment.hpp>
#include <godot_cpp/classes/mesh_instance3d.hpp>
#include <godot_cpp/classes/camera3d.hpp>
#include <godot_cpp/classes/standard_material3d.hpp>
#include <godot_cpp/classes/image_texture.hpp>
#include <godot_cpp/core/memory.hpp>
#include <vector>
#include <array>
#include <map>

#include "components/_interaction_node.h"
#include "components/dynamic_object.h"

namespace godot {

struct GltfDownloadState {
	bool json = false;
	bool bin = false;
};

struct RemoteTexture {
	bool loaded = false;
	bool downloading = false;
	std::vector<StandardMaterial3D*> materials = {};
	Ref<ImageTexture> texture;
};

// struct InteractionSettings {
// 	float transition_duration = 0.5f;
// 	float speed = 1.0f;
// 	float extent = 0.5f;
// 	int interaction_id = 0;
// };

struct ObjectSettings {
	std::array<float, 3> color = {1.0, 1.0, 1.0}; 
	std::array<float, 3> change_color = {1.0, 1.0, 1.0}; 
	// std::map<int, InteractionNode*> interactions;
	// float transition_duration = 0.5f;
	float speed = 1.0f;
	float extent = 0.5f;
	int interaction_id = 0;
};

class CmdProcessor : public Node {
	GDCLASS(CmdProcessor, Node)

private:
    UDPServer* server = memnew(UDPServer);
	std::vector<Ref<PacketPeerUDP>> peers;

	void handle_data_received(godot::String data);
	void cmd_sync(String data);
	void _gltf_json_request_completed(int result, int response_code, PackedStringArray headers, PackedByteArray body);
	void _gltf_bin_request_completed(int result, int response_code, PackedStringArray headers, PackedByteArray body);
	void _connect_request_completed(int result, int response_code, PackedStringArray headers, PackedByteArray body);
	void _texture_request_completed(int result, int response_code, PackedStringArray headers, PackedByteArray body);
	void cmd_update_bg(PackedStringArray data);
	void cmd_camera_location(PackedStringArray data);
	void cmd_camera_directions(PackedStringArray data);
	void cmd_camera_euler(PackedStringArray data);

	void set_background_color(Color color);
	void load_gltf();

	void set_object_properties(std::map<std::string, DynamicObject*> object_map, Node* node);

	WorldEnvironment* world;
	Camera3D* camera;

	GltfDownloadState* gltf_download_state = new GltfDownloadState();	

	Node3D* gltf_root = memnew(Node3D);

	bool handle_static_object_settings(Variant extras, DynamicObjectSettings* object_settings);
	InteractionNode* handle_dynamic_object_settings(Variant extras, const Variant &trigger);

	std::string host = "10.155.127.2";

	std::map<uint64_t, MeshInstance3D*> pending_nav_mesh_layer_checks = {};
	int nav_mesh_layer_countdown = 0;

	std::map<String, RemoteTexture*> remote_textures = {};

	void set_remote_texture(String texture_path, StandardMaterial3D* material);
	void apply_texture_to_material(Ref<Texture2D> texture, StandardMaterial3D* material);

protected:
	static void _bind_methods();

public:
	CmdProcessor();
	~CmdProcessor();

	void _process(double delta) override;
	void _ready() override;
};

}



#endif
