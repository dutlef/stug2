#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <regex>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/variant/string.hpp>
#include "tinyformat/tinyformat.h"

#include <stdlib.h>
#include <sys/types.h>
// #include <ifaddrs.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#include "android-ifaddrs/ifaddrs.h"

// https://stackoverflow.com/a/5888676
size_t split_string2(const std::string &txt, std::vector<std::string> &strs, char ch);

template<typename... Args>
void LOG(std::string str)
{
    // cannot see printf in logcat, so using this instead
    godot::UtilityFunctions::print(godot::String(str.c_str()));
}


template<typename... Args>
void LOG(const char* fmt, const Args&... args)
{
    auto str = tfm::format(fmt, args...);
    LOG(str);
}

int get_local_address();

int get_broadcast_addr();

struct NetworkInterface {
    std::string name = "";
    std::string inet = "";
};

std::vector<NetworkInterface> get_data_from_ifconfig_data(const std::string& ifconfig_text);

void test_get_data_from_ifconfig_data();

bool variant_matches_type(godot::Variant variant, godot::Variant::Type expected_type, bool log = false);

std::string str(godot::String string);
std::string str(godot::StringName string_name);
std::string str(godot::Dictionary dictionary);
std::string str(godot::Vector3 vector3);
std::string str(godot::Vector2 vector2);

const uint32_t NAV_MESH_LAYER = 0b00000000000000000000000000000010;