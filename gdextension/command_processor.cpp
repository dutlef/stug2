#include "command_processor.h"
#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/core/object.hpp>
#include <godot_cpp/core/memory.hpp>
#include <godot_cpp/classes/http_request.hpp>
#include <godot_cpp/classes/udp_server.hpp>
#include <godot_cpp/classes/gltf_document.hpp>
#include <godot_cpp/classes/gltf_state.hpp>
#include <godot_cpp/classes/packet_peer_udp.hpp>
#include <godot_cpp/classes/mesh_instance3d.hpp>
#include <godot_cpp/classes/ref.hpp>
#include <godot_cpp/classes/ref_counted.hpp>
#include <godot_cpp/classes/weak_ref.hpp>
#include <godot_cpp/classes/camera3d.hpp>
#include <godot_cpp/classes/node.hpp>
#include <godot_cpp/classes/world_environment.hpp>
#include <godot_cpp/classes/environment.hpp>
#include <godot_cpp/core/memory.hpp>
#include <godot_cpp/classes/json.hpp>
#include <godot_cpp/classes/file_access.hpp>
#include <godot_cpp/classes/material.hpp>
#include <godot_cpp/classes/mesh.hpp>
#include <godot_cpp/classes/mesh_instance3d.hpp>
#include <godot_cpp/classes/standard_material3d.hpp>
#include <godot_cpp/classes/object.hpp>
#include <godot_cpp/classes/static_body3d.hpp>
#include <godot_cpp/classes/rigid_body3d.hpp>
#include <godot_cpp/classes/collision_shape3d.hpp>
#include <godot_cpp/classes/box_shape3d.hpp>
#include <godot_cpp/classes/object.hpp>
#include <godot_cpp/classes/resource_loader.hpp>
#include <godot_cpp/classes/texture.hpp>
#include <godot_cpp/classes/image.hpp>
#include <godot_cpp/classes/texture2d.hpp>
#include <godot_cpp/classes/image_texture.hpp>

#include "components/no_interaction.h"
#include "components/float_in_space.h"
#include "components/play_animation.h"
#include "components/_interaction_node.h"
#include "components/dynamic_object.h"
#include "grabbable.h"
#include "has_initial_parent.h"


#include <stdio.h>
#include <functional>
#include <map>
#include <string>
// #include "tinyformat/tinyformat.h"
#include "util.h"

#include "gltf_extension.h"
#include "components/float_in_space.h"

using namespace godot;

void CmdProcessor::_bind_methods() {
    ClassDB::bind_method(D_METHOD("_gltf_bin_request_completed"), &CmdProcessor::_gltf_bin_request_completed);
    ClassDB::bind_method(D_METHOD("_gltf_json_request_completed"), &CmdProcessor::_gltf_json_request_completed);
    ClassDB::bind_method(D_METHOD("_connect_request_completed"), &CmdProcessor::_connect_request_completed);
    ClassDB::bind_method(D_METHOD("_texture_request_completed"), &CmdProcessor::_texture_request_completed);
}


CmdProcessor::CmdProcessor() {
}

CmdProcessor::~CmdProcessor() {
}

void CmdProcessor::_ready() {
    world = memnew(WorldEnvironment);
    Ref<Environment> env = memnew(Environment);
    env->set_background(Environment::BGMode::BG_COLOR);
    world->set_environment(env);

    add_child(gltf_root);

    add_child(world);
    // world.get_environment().instantiate()

    camera = memnew(Camera3D);
    camera->set_name("root camera");

    #ifndef XR
    camera->make_current();
    #endif

    add_child(camera);

    server->listen(34254);
    LOG("i am listen");

    // connect to blender addon
    auto http_request = memnew(HTTPRequest);
	add_child(http_request);
    http_request->connect("request_completed", Callable(this, "_connect_request_completed"));
	
    // TODO: automatically find address
	auto error = http_request->request(tfm::format("http://%s:8517/connect", host).c_str());
	if (error != Error::OK) {
		LOG("An error occurred in the HTTP request.");
    }

    LOG("requested");
}

void CmdProcessor::_process(double delta) {
    memnew(CmdProcessor);

    memnew(MeshInstance3D);

    // TODO: maybe change to tcp, idk
    UDPServer* a = memnew(UDPServer);

    server->poll();
    if (server->is_connection_available())
    {
        auto peer = server->take_connection();
        auto packet = peer->get_packet();
        auto ip = peer->get_packet_ip();
        auto port = peer->get_packet_port();
        LOG("Accepted peer: %s:%d", ip.utf8().get_data(), port);

        auto data = packet.get_string_from_utf8();
        handle_data_received(data);

        // peers.append(peer)
        peers.push_back(peer);
    }

    for (auto & peer : peers) {
        int32_t packets = peer->get_available_packet_count();
        for (int i = 0; i < packets; i++) {
            auto packet = peer->get_packet();
            auto data = packet.get_string_from_utf8();
            // printf("received: %s\n", data.utf8().get_data());
            handle_data_received(data);
        }
    }

    nav_mesh_layer_countdown--;

    if (nav_mesh_layer_countdown < 0)
    {

        auto it = pending_nav_mesh_layer_checks.begin();
        while (it != pending_nav_mesh_layer_checks.end()) 
        {
            auto current = it;
            auto node = current->second;
            ++it;
            for (int i = 0; i < node->get_child_count(); i++)
            {
                auto child = node->get_child(i);
                if (child->is_class("StaticBody3D")) {
                    auto sb = Object::cast_to<StaticBody3D>(child);
                    // sb->set_collision_mask(NAV_MESH_LAYER);
                    sb->set_collision_layer(NAV_MESH_LAYER);
                    LOG("set %s to nav layer", str(node->get_name()));
                    pending_nav_mesh_layer_checks.erase(current);
                }

            }
        }

        nav_mesh_layer_countdown = 50;
    }

    // auto a = memnew(UDPServer);

	// UDPServer server = memnew(String);
}

void CmdProcessor::handle_data_received(String data) {
    auto args = data.split("#");
	auto cmd_name = args[0];
	args.remove_at(0);
    // LOG("cmd: %s, arg size: %i", cmd_name.utf8().get_data(), args.size());
    // printf("calling %s\n", cmd_name.utf8().get_data());
    // printf("args %i\n", args.size());
	// auto callable = Callable(this, "cmd_" + cmd_name);
	// callable.callv(args);

    // switch (cmd_name)
    // {
    // case "sync":
    //     cmd_sync(args[0]);
    //     break;
    
    // default:
    //     break;
    // }
    if (cmd_name == "sync") {
        cmd_sync(args[0]);
    } else if (cmd_name == "update_bg") {
        cmd_update_bg(args);
    } else if (cmd_name == "camera_location") {
        cmd_camera_location(args);
    } else if (cmd_name == "camera_directions") {
        cmd_camera_directions(args);
    } else if (cmd_name == "camera_euler") {
        cmd_camera_euler(args);
    }
}


void CmdProcessor::cmd_sync(String ip) {
    LOG("start sync");
    host = std::string(ip.utf8().get_data());
    gltf_download_state->bin = false;
    gltf_download_state->json = false;

    // .bin file
    auto bin_request = memnew(HTTPRequest);
	add_child(bin_request);

    bin_request->connect("request_completed", Callable(this, "_gltf_bin_request_completed"));
	bin_request->set_download_file("user://scene.bin");
	
	auto bin_error = bin_request->request(tfm::format("http://%s:8517/static/scene.bin", host).c_str());
	if (bin_error != Error::OK) {
		LOG("An error occurred in the HTTP request.");
    }

    // .gltf file
    auto json_request = memnew(HTTPRequest);
	add_child(json_request);

    json_request->connect("request_completed", Callable(this, "_gltf_json_request_completed"));
	json_request->set_download_file("user://scene.gltf");
	
	auto json_error = json_request->request(tfm::format("http://%s:8517/static/scene.gltf", host).c_str());
	if (json_error != Error::OK) {
		LOG("An error occurred in the HTTP request.");
    }
}


void CmdProcessor::_gltf_bin_request_completed( int result, int response_code, PackedStringArray headers, PackedByteArray body ) {
    LOG("[gltf bin] result: %i, response code: %i", result, response_code);
    gltf_download_state->bin = true;

    if (gltf_download_state->json) {
    	load_gltf();
    }
}

void CmdProcessor::_gltf_json_request_completed( int result, int response_code, PackedStringArray headers, PackedByteArray body ) {
    LOG("[gltf json] result: %i, response code: %i", result, response_code);
    gltf_download_state->json = true;

    if (gltf_download_state->bin) {
    	load_gltf();
    }
}


void CmdProcessor::load_gltf() {
    auto gltf_document_load = memnew(GLTFDocument);

    // TODO: figure out how this works
    // auto ext = memnew(CustomGLTFExtension);
    // gltf_document_load->register_gltf_document_extension(ext, true);

	Ref<GLTFState> gltf_state_load = memnew(GLTFState);
	auto error = gltf_document_load->append_from_file("user://scene.gltf", gltf_state_load);

	
	if (error != Error::OK) {
		LOG("Couldn't load glTF scene (error code: %i).", error);
        return;
    }

    for (int i = 0; i < gltf_root->get_child_count(); i++) {
        auto node = gltf_root->get_child(i);
        gltf_root->remove_child(node);
        // TODO: what happens with orphans?
    }

    auto gltf_scene_root_node = gltf_document_load->generate_scene(gltf_state_load);
    gltf_root->add_child(gltf_scene_root_node);

    // on my mobile device the gltf camera was made the current
    // but i want the original one to remain current
    #ifndef XR
    camera->make_current();
    #endif

    auto file = godot::FileAccess::open("user://scene.gltf", FileAccess::READ);
    String text = file->get_as_text();

    auto json = memnew(JSON);
    auto json_error = json->parse(text);

    if (json_error != Error::OK) {
        LOG("Error parsing json: '%s' at line %i", json->get_error_message().utf8().get_data(), json->get_error_line());
        return;
    }

    auto data = json->get_data();
    LOG("haskey: %i; has %i", data.has_key("nodes"), data.has_key("shdwe"));
    auto nodes = data.get(Variant(String("nodes")));
    auto type = nodes.get_type();

    if (type != Variant::Type::ARRAY) {
        auto type_name = Variant::get_type_name(type);
        LOG("Type of nodes should be array, instead is %s", type_name.utf8().get_data());
        return;
    }

    Array nodes_array = nodes;
    LOG("node size %i", nodes_array.size());

    
    // std::map<char, float[3]> object_color_map = {};
    std::map<std::string, std::array<float, 3>> object_color_map;
    std::map<std::string, DynamicObject*> object_map;

    for (int i = 0; i < nodes_array.size(); i++)
    {
        auto node = nodes_array[i];
        auto dynamic_object = memnew(DynamicObject);
        auto object_settings = dynamic_object->settings; // TODO: free (idk how that works yet)

        auto name = node.get("name");
        if (!variant_matches_type(name, Variant::Type::STRING)) {
            continue;
        }

        // dots are not valid in godot node names
        auto name_str = std::string(((String) name).replace(".", "_").utf8().get_data());
        LOG("name: %s", name_str);

        dynamic_object->set_name(
            (std::string("DO_") + name_str).c_str()
        );

        auto mesh_index = node.get("mesh");
        if (variant_matches_type(mesh_index, Variant::Type::FLOAT)) {
            object_settings->is_mesh = true;
        }

        // blender's custom properties are called "extras" in gltf lingo
        auto extras = node.get("extras");

        handle_static_object_settings(extras, object_settings);

        for (auto &trigger : {"b2vr_on_begin", "b2vr_on_proximity_enter"})
        {
            InteractionNode* interaction = handle_dynamic_object_settings(extras, Variant(trigger));
            interaction->trigger = std::string(trigger);
            object_settings->interactions.insert({std::string(trigger), interaction});
        }
        // InteractionNode* interaction = handle_dynamic_object_settings(extras, "b2vr_on_begin");
        // interaction->trigger = std::string("b2vr_on_begin");
        // object_settings->interactions.insert({std::string("b2vr_on_begin"), interaction});

        // InteractionNode* interaction2 = handle_dynamic_object_settings(extras, "b2vr_on_proximity_enter");
        // interaction2->trigger = std::string("b2vr_on_proximity_enter");
        // object_settings->interactions.insert({std::string("b2vr_on_proximity_enter"), interaction2});

        for (auto const& x : object_settings->interactions)
        {
            auto trigger = x.first;
            auto interaction_node = x.second;
            LOG("       aha  %s => %s", trigger, str(interaction_node->get_name()));

            // if (x.first == std::string("b2vr_on_begin")) {
            // 	interaction_node->enable();
            // }
        }


        object_map.insert({name_str, dynamic_object});
    }

    // log all colors
    // for(auto it = object_color_map.begin(); it != object_color_map.end(); ++it) {
    //     LOG("key :%s: value (%f,%f,%f)\n", it->first, it->second[0], it->second[1], it->second[2]);
    // }

    // traverse gltf scene
    for (int i = 0; i < gltf_scene_root_node->get_child_count(); i++) {
        auto child = gltf_scene_root_node->get_child(i);
        set_object_properties(object_map, child);
    }

    LOG("done with set_object_properties");

    for (auto &[key, object] : object_map)
    {
        LOG("doing %s", str(object->get_name()));
        object->trigger_interaction_start();
        LOG("done %s", str(object->get_name()));
    }

    LOG("done with trigger_interaction_start");
}

bool CmdProcessor::handle_static_object_settings(Variant extras, DynamicObjectSettings* object_settings) {
    LOG("a1");
    if (!variant_matches_type(extras, Variant::Type::DICTIONARY)) {
        // node has no extras
        return false;
    }

    auto b2vr_custom_object_properties = extras.get("b2vr_custom_object_properties");
    if (!variant_matches_type(b2vr_custom_object_properties, Variant::Type::DICTIONARY)) {
        LOG("node has no properties created by the addon");
        return false;
    }

    auto _object_color = b2vr_custom_object_properties.get("object_color");
    if (variant_matches_type(_object_color, Variant::Type::ARRAY)) {

        Array object_color = (Array) _object_color;

        if (object_color.size() != 3) {
            LOG("Expected object_color.size() to be 3, instead got %i", object_color.size());
        } else {
            object_settings->color[0] = object_color[0];
            object_settings->color[1] = object_color[1];
            object_settings->color[2] = object_color[2];
        }


    }

    auto texture_path = b2vr_custom_object_properties.get("texture_path");
    if (variant_matches_type(texture_path, Variant::Type::STRING)) {
        object_settings->texture_path = String(texture_path);
    }

    auto use_gravity = b2vr_custom_object_properties.get("use_gravity");
    if (variant_matches_type(use_gravity, Variant::Type::FLOAT)) {
        object_settings->use_gravity = (bool) use_gravity;
    }

    auto is_collider = b2vr_custom_object_properties.get("is_collider");
    if (variant_matches_type(is_collider, Variant::Type::FLOAT)) {
        object_settings->is_collider = (bool) is_collider;
    }

    auto grabbable = b2vr_custom_object_properties.get("grabbable");
    if (variant_matches_type(grabbable, Variant::Type::FLOAT)) {
        object_settings->grabbable = (bool) grabbable;
        LOG("... is grabbable %f", (float) grabbable);
    }

    auto remote_grab = b2vr_custom_object_properties.get("remote_grab");
    if (variant_matches_type(remote_grab, Variant::Type::FLOAT)) {
        object_settings->remote_grab = (bool) remote_grab;
    }

    auto nav_mesh = b2vr_custom_object_properties.get("is_nav_mesh");
    if (variant_matches_type(nav_mesh, Variant::Type::FLOAT)) {
        object_settings->is_nav_mesh = (bool) nav_mesh;
        LOG("... is nav mesh %f", (float) nav_mesh);
    }


    return true;
}



InteractionNode* CmdProcessor::handle_dynamic_object_settings(Variant extras, const Variant &trigger) {
    InteractionNode* interaction = memnew(NoInteraction);

    interaction->set_name("NoInteraction");

    if (!variant_matches_type(extras, Variant::Type::DICTIONARY)) {
        // node has no extras
        return interaction;
    }


    auto settings = extras.get(trigger);
    if (!variant_matches_type(settings, Variant::Type::DICTIONARY, true)) {
        return interaction;
    }

    int presets = 0; // which type of reaction it is

    LOG("checking presets");
    Variant presets_v = settings.get("presets");
    // for some reason it's a float, idk why
    if (variant_matches_type(presets_v, Variant::Type::FLOAT, true)) {
        presets = (int) presets_v;
        LOG("Presets changed to %i", presets);
    }

    switch (presets)
    {
    case 2:
        interaction = memnew(FloatInSpace);
        interaction->set_name("FloatInSpace");
        break;

    case 3:
        interaction = memnew(PlayAnimation);
        interaction->set_name("PlayAnimation");
        break;
    
    default:
        break;
    }

    // if (presets == 2) {
        
    // }

    // if (presets == 2) {
    //     interaction = memnew(FloatInSpace);
    //     interaction->set_name("FloatInSpace");
    // }

    Variant extent = settings.get("extent");
    if (variant_matches_type(extent, Variant::Type::FLOAT)) {
        // TODO: can it be INT?
        interaction->extent = (float) extent;
    }

    Variant speed = settings.get("speed");
    if (variant_matches_type(speed, Variant::Type::FLOAT)) {
        interaction->speed = (float) speed;
    }

    Variant animation_name = settings.get("animation_name");
    if (variant_matches_type(animation_name, Variant::Type::STRING)) {
        interaction->string_a = String(animation_name).replace(".", "_");
    }

    Variant object_type = settings.get("object_type");
    if (variant_matches_type(object_type, Variant::Type::FLOAT)) {
        interaction->object_type_index = (int) object_type;
    }

    Variant object_name = settings.get("object_name");
    if (variant_matches_type(object_name, Variant::Type::STRING)) {
        interaction->object_name = String(object_name).replace(".", "_");
    }

    interaction->set_gltf_root(gltf_root);

    return interaction;
}



void CmdProcessor::set_object_properties(std::map<std::string, DynamicObject*> object_map, Node* node) {
    for (int i = 0; i < node->get_child_count(); i++) {
        auto child = node->get_child(i);
        set_object_properties(object_map, child);
    }
    auto name = std::string(node->get_name().get_slice("@", 0).utf8().get_data());
    LOG("child :%s:(%s) in_tree: %f", name, str(node->get_name()), node->is_inside_tree());

    
    auto it = object_map.find(name);
    bool exists = it != object_map.end();

    if (object_map.count(name) == 0) {
        return;
    }

    auto dynamic_object = object_map[name];
    auto settings = dynamic_object->settings;
    node->add_child(dynamic_object);

    LOG("TEXTURE PATH: %s", str(settings->texture_path));

    if (!settings->is_mesh) {
        return;
    }

    auto rgb = settings->color;
    auto mesh_instance = Object::cast_to<MeshInstance3D>(node);

    Color new_color = Color(1.0, 0.0, 0.0); // Red color
    StandardMaterial3D* new_material = memnew(StandardMaterial3D);
    new_material->set_albedo(Color(rgb[0], rgb[1], rgb[2]));
    mesh_instance->set_material_override(new_material);

    if (settings->texture_path != String(""))
    {
        set_remote_texture(settings->texture_path, new_material);
    }

    if (settings->is_collider && !settings->use_gravity &!settings->is_nav_mesh) {
        mesh_instance->create_multiple_convex_collisions();
    }

    Node3D* root = mesh_instance;
    MeshInstance3D* root_mesh_instance = mesh_instance;

    if (settings->is_nav_mesh)
    {
        // mesh_instance->create_multiple_convex_collisions();
        mesh_instance->create_trimesh_collision();
        auto id = mesh_instance->get_instance_id();
        pending_nav_mesh_layer_checks.insert({id, mesh_instance});
        mesh_instance->set_visible(false);
        // LOG("mesh instance %s is a nav mesh", str(root_mesh_instance->get_name()));
        // auto collision_object = memnew(StaticBody3D);
        // mesh_instance->add_child(collision_object);
        // collision_object->set_collision_layer(NAV_MESH_LAYER);
        // collision_object->set_collision_mask(NAV_MESH_LAYER);

        // auto collider = memnew(CollisionShape3D);
        // collider->set_shape(mesh_instance->get_mesh());
        // collision_object->add_child(collider);
    }

    if (settings->use_gravity) {
        LOG("mesh %s has gravity", String(mesh_instance->get_name()).utf8().get_data());
        auto parent = mesh_instance->get_parent();

        auto rb = memnew(RigidBody3D);
        auto rb_name = String("RB_") + mesh_instance->get_name();

        mesh_instance->set_name("for deletion");
        rb->set_name(rb_name);
        rb->set_position(mesh_instance->get_position());
        rb->set_rotation(mesh_instance->get_rotation());
        parent->add_child(rb);

        root = rb;

        // parent->remove_chisld(mesh_instance);

        // auto new_mi = Object::cast_to<MeshInstance3D>(mesh_instance->duplicate());
        auto new_mi = memnew(MeshInstance3D);
        new_mi->set_mesh(mesh_instance->get_mesh());
        new_mi->set_scale(mesh_instance->get_scale());
        new_mi->set_material_override(new_material);
        root_mesh_instance = new_mi;


        for (int i = 0; i < mesh_instance->get_child_count(); i++)
        {
            auto child = mesh_instance->get_child(i);
            String _class = child->get_class();
            LOG("child has class %s", _class.utf8().get_data());

            if (child->is_class("StaticBody3D"))
            {   
                // TODO: is there a callback for when static body has been generated from create_multiple_convex_collisions()?
                LOG("Static body %s has %i children", String(child->get_name()).utf8().get_data(), child->get_child_count());
                for (int j = 0; j < child->get_child_count(); j++)
                {
                    auto shape = mesh_instance->get_child(j);
                    shape->reparent(rb);
                }
            }
            else {
                child->reparent(rb);
            }
        }
        

        rb->add_child(new_mi);

        AABB aabb = new_mi->get_aabb();

        auto collision = memnew(CollisionShape3D);
        Ref<BoxShape3D> box = memnew(BoxShape3D);
        box->set_size(aabb.get_size() * new_mi->get_scale());
        collision->set_shape(box);

        rb->add_child(collision);

        mesh_instance->queue_free();
        LOG("for deletion: %s %i", str(mesh_instance->get_name()), mesh_instance->get_instance_id());
    }

    if (settings->grabbable && !settings->is_nav_mesh)
    {
        LOG("mesh instance %s is grabbable", str(root_mesh_instance->get_name()));
        auto grabbable = memnew(Grabbable);
        grabbable->set_mesh_instance(root_mesh_instance);
        root->add_child(grabbable);

        auto has_initial_parent = memnew(HasInitialParent);
        root->add_child(has_initial_parent);
    }

}

void CmdProcessor::_connect_request_completed( int result, int response_code, PackedStringArray headers, PackedByteArray body ) {
    if (result != HTTPRequest::RESULT_SUCCESS) {
        return;
    }

    String json_text = body.get_string_from_utf8();
    auto json = memnew(JSON);

    auto error = json->parse(json_text);
    if (error == !Error::OK) {
        LOG("Error parsing json.");
        return;
    }

    auto data = json->get_data();

    auto bg = data.get("background_color");
    auto r = bg.get("r");
    auto g = bg.get("g");
    auto b = bg.get("b");
    set_background_color(Color(r, g, b));

    // load_gltf();
}

void CmdProcessor::cmd_update_bg(PackedStringArray data) {
    auto bg = Color(data[0].to_float(), data[1].to_float(), data[2].to_float());
    set_background_color(bg);
}

void CmdProcessor::set_background_color(Color color) {
    auto world_environment = world->get_environment();
    if (world_environment.is_valid()) {
        Color col = world_environment->get_bg_color();
        world_environment->set_bg_color(color);
    } else {
        LOG("invalid world environment");
    }
}

void CmdProcessor::cmd_camera_location(PackedStringArray data) {
    // TODO: iuuuuuu
    auto split = data[0].split(",");
    auto x = split[0].replace("[", "").to_float();
    auto y = split[1].to_float();
    auto z = split[2].replace("]", "").to_float();
    camera->set_position(Vector3(x, y, z));
}
	
void CmdProcessor::cmd_camera_directions(PackedStringArray data) {
    // TODO: igitt
    auto d = data[0].replace("[", "").replace("]", "").split(",");
	auto forward = Vector3(d[0].to_float(), d[1].to_float(), d[2].to_float());
	auto up = Vector3(d[3].to_float(), d[4].to_float(), d[5].to_float());
	Vector3 target = camera->get_position() + forward;
	camera->look_at(target, up);
}

void CmdProcessor::cmd_camera_euler(PackedStringArray data) {
    auto split = data[0].split(",");
    auto x = split[0].replace("[", "").to_float();
    auto y = split[1].to_float();
    auto z = split[2].replace("]", "").to_float();

    camera->set_rotation(Vector3(x, y, z));
}

void CmdProcessor::set_remote_texture(String texture_path, StandardMaterial3D* material)
{

    auto exists = remote_textures.find(texture_path) != remote_textures.end();

    if (exists)
    {
        auto rt = remote_textures[texture_path];

        if (rt->loaded)
        {
            apply_texture_to_material(rt->texture, material);
        }
        else if (rt->downloading)
        {
            rt->materials.push_back(material);
        }
    } 
    else 
    {
        auto rt = new RemoteTexture();
        rt->downloading = true;
        rt->materials.push_back(material);
        remote_textures.insert({texture_path, rt});
        
        auto request = memnew(HTTPRequest);
        add_child(request);

        request->connect("request_completed", Callable(this, "_texture_request_completed"));

        auto download_path = texture_path.replace("//b2vr_data/", "user://");
        request->set_download_file(download_path);
    
        auto root = String(tfm::format("http://%s:8517/static/", host).c_str());
        auto url = texture_path.replace("//b2vr_data/", root);
        LOG("texture url: %s", str(url));
        auto error = request->request(url);
        if (error != Error::OK) {
            LOG("An error occurred in the HTTP request.");
        }

    }
}

void CmdProcessor::_texture_request_completed( int result, int response_code, PackedStringArray headers, PackedByteArray body) {
    LOG("[texture] result: %i, response code: %i, headers: %i", result, response_code, headers.size());


    for (int i = 0; i < headers.size(); i++)
    {
        String header = headers[i];
        LOG("  %s", str(header));
        if (header.contains("B2VR-File-Name"))
        {
            String texture_path = header.replace("B2VR-File-Name: ", "//b2vr_data/");
            LOG("    searching: %s", str(texture_path)); 

            auto exists = remote_textures.find(texture_path) != remote_textures.end();
            if (!exists)
            {
                LOG("Warning: invalid texture path.");
                continue;
            }

            String local_path = header.replace("B2VR-File-Name: ", "user://");

            auto img = Image::load_from_file(local_path);
            // img->srgb_to_linear();
            // img->rgbe_to_srgb();
            auto texture = ImageTexture::create_from_image(img);

            auto rt = remote_textures[texture_path];
            rt->loaded = true;
            rt->downloading = false;
            rt->texture = texture;

            for (auto &material : rt->materials)
            {
                LOG("set material");
                // material->set_texture(
                //     BaseMaterial3D::TextureParam::TEXTURE_ALBEDO,
                //     texture
                // );
                apply_texture_to_material(texture, material);
            }
        }
    }
}

void CmdProcessor::apply_texture_to_material(Ref<Texture2D> texture, StandardMaterial3D* material)
{
    material->set_specular(0);
    material->set_albedo(Color(0, 0, 0));
    material->set_feature(BaseMaterial3D::FEATURE_EMISSION, true);
    material->set_emission(Color(0, 0, 0));
    material->set_texture(BaseMaterial3D::TextureParam::TEXTURE_EMISSION, texture);
}