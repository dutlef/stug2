#pragma once

#include "hand.h"
#include "scene_root.h"
#include <godot_cpp/classes/node3d.hpp>
#include <godot_cpp/classes/xr_origin3d.hpp>
#include <godot_cpp/classes/xr_controller3d.hpp>
#include <godot_cpp/classes/xr_camera3d.hpp>
#include <godot_cpp/classes/mesh_instance3d.hpp>

namespace godot {

class Grabbable : public Node3D {
	GDCLASS(Grabbable, Node3D)

private:
    MeshInstance3D* mesh_instance;
	Hand* left_hand;
	Hand* right_hand;
	SceneRoot* root;
	Node3D* initial_parent;
	Vector3 initial_scale;

	bool any_hand_within_distance;
	bool being_held_left;
	bool being_held_right;
	bool left_close;
	bool right_close;

    float threshold_squared = 0.25f;

protected:
	static void _bind_methods();

public:
	Grabbable();
	~Grabbable();

	void _process(double delta) override;
	void _ready() override;

	void set_mesh_instance(MeshInstance3D* mesh_instance);

	bool get_being_held();

};
}

