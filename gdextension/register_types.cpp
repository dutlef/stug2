#include "register_types.h"

#include "gdexample.h"
#include "scene_root.h"

#include "components/dynamic_object.h"
#include "components/_interaction_node.h"
#include "components/float_in_space.h"
#include "components/play_animation.h"
#include "components/no_interaction.h"

#include "custom_xr_origin.h"
#include "hand.h"
#include "grabbable.h"
#include "has_initial_parent.h"

#include "b2vr_test.h"
#include "command_processor.h"

#include <gdextension_interface.h>
#include <godot_cpp/core/defs.hpp>
#include <godot_cpp/godot.hpp>

using namespace godot;

void initialize_example_module(ModuleInitializationLevel p_level) {
	if (p_level != MODULE_INITIALIZATION_LEVEL_SCENE) {
		return;
	}

	ClassDB::register_class<GDExample>();
	ClassDB::register_class<SceneRoot>();

	ClassDB::register_class<DynamicObject>();
	ClassDB::register_class<InteractionNode>();
	ClassDB::register_class<FloatInSpace>();
	ClassDB::register_class<PlayAnimation>();
	ClassDB::register_class<NoInteraction>();

	ClassDB::register_class<CustomXROrigin>();
	ClassDB::register_class<Hand>();
	ClassDB::register_class<Grabbable>();
	ClassDB::register_class<HasInitialParent>();

	ClassDB::register_class<B2VRTest>();
	ClassDB::register_class<CmdProcessor>();
}

void uninitialize_example_module(ModuleInitializationLevel p_level) {
	if (p_level != MODULE_INITIALIZATION_LEVEL_SCENE) {
		return;
	}
}

extern "C" {
// Initialization.
GDExtensionBool GDE_EXPORT example_library_init(GDExtensionInterfaceGetProcAddress p_get_proc_address, const GDExtensionClassLibraryPtr p_library, GDExtensionInitialization *r_initialization) {
	godot::GDExtensionBinding::InitObject init_obj(p_get_proc_address, p_library, r_initialization);

	init_obj.register_initializer(initialize_example_module);
	init_obj.register_terminator(uninitialize_example_module);
	init_obj.set_minimum_library_initialization_level(MODULE_INITIALIZATION_LEVEL_SCENE);

	return init_obj.init();
}
}