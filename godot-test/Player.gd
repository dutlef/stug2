extends Node3D

@export var xr_rig: PackedScene
@export var desktop_rig: PackedScene

func _ready():
	if OS.get_name() == "Linux":
		add_child(desktop_rig.instantiate())
	else:
		add_child(xr_rig.instantiate())

func _process(delta):
	pass
