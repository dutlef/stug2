class_name CommandProcessor
extends Node

var server := UDPServer.new()
var peers = []

var on_desktop := false
var camera

func _ready():
	server.listen(34254)
	if OS.get_name() == "Linux":
		on_desktop = true

func _process(delta):
	server.poll() # Important!
	if server.is_connection_available():
		var peer: PacketPeerUDP = server.take_connection()
		var packet = peer.get_packet()
		print("Accepted peer: %s:%s" % [peer.get_packet_ip(), peer.get_packet_port()])
		#print("Received data: %s" % [packet.get_string_from_utf8()])
		handle_data_received(packet.get_string_from_utf8())
		#peer.put_packet(packet)
		peers.append(peer)

	for i in range(0, peers.size()):
		var peer = peers[i]
		for j in range(0, peer.get_available_packet_count()):
			var packet = peer.get_packet()
			handle_data_received(packet.get_string_from_utf8())

func handle_data_received(data: String):
	var args = data.split("#")
	var cmd_name = args[0]
	args.remove_at(0)
	var callable = Callable(self, "cmd_" + cmd_name)
	callable.callv(args)
	
func cmd_sync(path: String):
	var http_request = HTTPRequest.new()
	add_child(http_request)
	
	http_request.request_completed.connect(self._gltf_request_completed)
	http_request.set_download_file("user://scene.glb")
	
	var error = http_request.request("http://localhost:8517/scene.glb")
	if error != OK:
		push_error("An error occurred in the HTTP request.")
	
	
func _gltf_request_completed(result, response_code, headers, body):
	var gltf_document_load = GLTFDocument.new()
	var gltf_state_load = GLTFState.new()
	#gltf_state_load.set_handle_binary_image(GLTFState.HANDLE_BINARY_EMBED_AS_BASISU)
	var error = gltf_document_load.append_from_file("user://scene.glb", gltf_state_load)
	
	if error == OK:
		var gltf_scene_root_node = gltf_document_load.generate_scene(gltf_state_load)
		add_child(gltf_scene_root_node)
	else:
		print("Couldn't load glTF scene (error code: %s)." % error_string(error))

	
func cmd_update_bg(r: String, g: String, b: String):
	print(r, g, b)
	$"../WorldEnvironment".environment.set_bg_color(Color(float(r), float(g), float(b)))
	
var json = JSON.new()
func cmd_view_matrix(mat: String):
	if not on_desktop:
		return
	var mat2 = str_to_var(mat)
	#print(str_to_var(mat))
	#print("*********")
	#print(mat2[0])
	#print("-----")
	#var proj = Projection(mat2[0], mat2[1], mat2[2], mat2[3])
	#print(proj)
	#print("=")
	#print("=")
	#get_viewport().get_camera_3d()
	
	
func cmd_camera_location(pos: String):
	var cam := get_viewport().get_camera_3d()
	var p = str_to_var(pos)
	cam.position.x = p[0]
	cam.position.y = p[1]
	cam.position.z = p[2]
	
func cmd_camera_quaternion(directions: String):
	var cam := get_viewport().get_camera_3d()
	print(cam.position)
	var d = str_to_var(directions)
	print(d)
	
func cmd_camera_directions(dirs: String):
	var d = str_to_var(dirs)
	print(d)
	var forward = Vector3(d[0], d[1], d[2])
	var up = Vector3(d[3], d[4], d[5])
	var cam := get_viewport().get_camera_3d()
	var target = cam.position + forward
	cam.look_at(target, up)
