#!/usr/bin/env bash

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
echo "Script directory: $SCRIPT_DIR"

cd $SCRIPT_DIR/..
scons platform=android verbose=yes || exit 1

cd $SCRIPT_DIR/../godot-test

godot --headless --export-debug "Android" lol.apk

adb install lol.apk || exit 1

adb shell monkey -p com.example.lalala -c android.intent.category.LAUNCHER 1

adb logcat | grep godot
