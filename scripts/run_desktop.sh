#!/usr/bin/env bash

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
echo "Script directory: $SCRIPT_DIR"

cd $SCRIPT_DIR/..
scons || exit 1

cd $SCRIPT_DIR/../godot-test
godot